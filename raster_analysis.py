# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_reclass.ui'
#
# Created: Mon Jan 20 11:32:19 2014
#      by: PyQt4 UI code generator 4.10.3
#
# WARNING! All changes made in this file will be lost!

from osgeo import gdal, gdalnumeric, ogr, osr
import Image, ImageDraw
import os, sys
gdal.UseExceptions()
try:
    import qgis.analysis
except:
    print "librerie qgis non trovate"
    
    
class raster_udp:
    def __init__(self,shapefile_path,raster_path):
        self.shapefile_path=shapefile_path
        self.raster_path=raster_path        
    
    def imageToArray(self,i):
        """        
        Converte un immagine python in un immagine gdalnumeric
        """
        a=gdalnumeric.fromstring(i.tostring(),'b')
        a.shape=i.im.size[1], i.im.size[0]
        return a


    def arrayToImage(self,a):
        """
        Converte un immagine gdalnumeric in un immagine standard 
        """
        self.a_array=a
        self.i_array=Image.fromstring('L',(self.a_array.shape[1],self.a_array.shape[0]),
                (self.a_array.astype('b')).tostring())
        return self.i_array

    def world2Pixel(self,geoMatrix, x, y):
        self.geoMatrix=geoMatrix
        self.x_w2p=x
        self.y_w2p=y

        """
        Uses a gdal geomatrix (gdal.GetGeoTransform()) to calculate
        the pixel location of a geospatial coordinate
        """
        #originex
        self.ulX = self.geoMatrix[0]
        #origine y
        self.ulY = self.geoMatrix[3]
        #dimenzione pixel x
        self.xDist = self.geoMatrix[1]
        #dimenzione pixel x
        self.yDist = self.geoMatrix[5]
        
        self.rtnX = self.geoMatrix[2]

        self.rtnY = self.geoMatrix[4]

        self.pixel = int((self.x_w2p - self.ulX) / self.xDist)
        #pixel rappresenta il numero di righe
        self.line = int((self.ulY - self.y_w2p) / self.xDist)
        #line rappresenta il numero di colonne
        return (self.pixel, self.line)

    def OpenArray( self, array, prototype_ds = None, xoff=0, yoff=0 ):
        self.array_open=array
        self.prototype_ds=prototype_ds
        self.xoff=xoff
        self.yoff=yoff
        
        self.ds_oa = gdal.Open( gdalnumeric.GetArrayFilename(self.array_open) )
    
        if self.ds_oa is not None and self.prototype_ds is not None:
            if type(self.prototype_ds).__name__ == 'str':
                self.prototype_ds = gdal.Open( self.prototype_ds )
            if self.prototype_ds is not None:
                gdalnumeric.CopyDatasetInfo( self.prototype_ds, self.ds_oa, xoff=self.xoff, yoff=self.yoff )
        return self.ds_oa
    
    def analizza_raster(self,tipoanalisi,areatot):
        self.tipoanalisi=tipoanalisi
        self.areatot=areatot

        self.diz_udp_raster=dict()

        # legge il file raster come un vettore gdalnumeric
        self.srcArray = gdalnumeric.LoadFile(self.raster_path)
    

        self.srcImage = gdal.Open(self.raster_path)
        self.geoTrans = self.srcImage.GetGeoTransform()

        
        self.shapef = ogr.Open(self.shapefile_path)
        #self.lyr = self.shapef.GetLayer( os.path.split( os.path.splitext( self.shapefile_path )[0] )[1] )
        self.lyr = self.shapef.GetLayer()

        if self.tipoanalisi=="aspect":
            self.result_analizzaraster="\nKAPPA ASPECT ANALYSIS\n\n"
        if self.tipoanalisi=="clima":
            self.result_analizzaraster="\nKAPPA CLIMATE ANALYSIS\n\n"
        if self.tipoanalisi=="soil":
            self.result_analizzaraster="\nKAPPA SOIL ANALYSIS\n\n"

        #da qui inizia il ciclo
        
        self.poly_raster = self.lyr.GetNextFeature()

        while self.poly_raster:
          self.geomp=self.poly_raster.GetGeometryRef() 
          self.geompa = self.geomp.Clone()
          self.polyarea = self.geompa.GetArea()      
          self.attr_lu=self.poly_raster.GetField("belu")
          

          
          # Convert the layer extent to image pixel coordinates
          self.minX, self.maxX, self.minY, self.maxY = self.lyr.GetExtent()
          self.ulX, self.ulY = self.world2Pixel(self.geoTrans, self.minX, self.maxY)
          self.lr_X, self.lr_Y = self.world2Pixel(self.geoTrans, self.maxX, self.minY)
          self.ulX=0
          self.ulY=0
    
          # Calculate the pixel size of the new image
          self.pxWidth = int(self.lr_X - self.ulX)
          self.pxHeight = int(self.lr_Y - self.ulY)
    
          self.clip = self.srcArray[self.ulY:self.lr_Y, self.ulX:self.lr_X]
    
          #
          # EDIT: create pixel offset to pass to new image Projection info
          #
          self.xoffset =  self.ulX
          self.yoffset =  self.ulY
    
          # Create a new geomatrix for the image
          self.geoTrans = list(self.geoTrans)
          self.geoTrans[0] = self.minX
          self.geoTrans[3] = self.maxY
    
          # Map points to pixels for drawing the
          # boundary on a blank 8-bit,
          # black and white, mask image.
          self.points = []
          self.pixels = []
          self.geom = self.poly_raster.GetGeometryRef()
          self.pts = self.geom.GetGeometryRef(0)
          for self.p in range(self.pts.GetPointCount()):
              self.points.append((self.pts.GetX(self.p), self.pts.GetY(self.p)))
          for self.p in self.points:
              self.pixels.append(self.world2Pixel(self.geoTrans, self.p[0], self.p[1]))

          self.rasterPoly = Image.new("L", (self.pxWidth, self.pxHeight), 1)
          self.rasterize = ImageDraw.Draw(self.rasterPoly)
          self.rasterize.polygon(self.pixels, 0)
          self.mask = self.imageToArray(self.rasterPoly)
          
          
          # Clip the image using the mask
          self.clip = gdalnumeric.choose(self.mask,(self.clip, 0)).astype(gdalnumeric.uint8)
    
          self.cont1=0
          self.cont0_5=0
          self.cont0=0
          
          self.lunghezza=len(self.clip[0])
          for self.value_raster in self.clip:
            for self.valore_raster in self.value_raster:
              if float(self.valore_raster)==1.0:
                self.cont1+=1
              if float(self.valore_raster)==0.5:
                self.cont0_5+=1
              if float(self.valore_raster)==0.0:
                self.cont0+=1

          self.sup1=self.geoTrans[1]*self.geoTrans[1]*self.cont1
          self.sup0_5=self.geoTrans[1]*self.geoTrans[1]*self.cont0_5
          self.sup0=self.geoTrans[1]*self.geoTrans[1]*self.cont0            
          

          
          self.ke=(self.sup1*1+self.sup0_5*0.5)
          self.ke=self.ke/self.polyarea
          
          #problema risoluzione spaziale raster
          #controllo maggiore di 1
          if self.ke>1:
              self.ke=1
          #print "indice kappaE: %s" % self.ke
          
          self.result_analizzaraster+="BELU code %s " %self.attr_lu
          self.result_analizzaraster+="kappa = %s" %self.ke  
          self.result_analizzaraster+="\n"
          self.diz_udp_raster[int(self.attr_lu)]=self.ke

          self.poly_raster = self.lyr.GetNextFeature()
        
      
        self.result_analizzaraster+="\n---------------------------\n"      
        
        
        
        return self.result_analizzaraster,self.diz_udp_raster