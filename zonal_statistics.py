# -*- coding: utf-8 -*-
"""
Created on Wed May 14 15:09:22 2014

@author: pietro
"""

import numpy as np


def report(zones, *themes):
    """Dbdqsdas
    
    >>> zones = np.array([[2, 2, 2],
    ...                   [2, 1, 1],
    ...                   [1, 1, 2],
    ...                   [1, 2, 2]])
    >>> theme = np.array([[6, 5, 8],
    ...                   [5, 7, 8],
    ...                   [8, 8, 8],
    ...                   [8, 8, 6]])
    >>> report(zones, theme)
    (5, 1, 0)
    (6, 1, 0)
    (7, 1, 1)
    (8, 1, 4)
    (5, 2, 2)
    (6, 2, 2)
    (7, 2, 0)
    (8, 2, 3)
    """
    # category mask
    mcats = {}
    # zone mask
    mzones = {}
    for zone in sorted(set(zones.flatten())):
        for theme in themes:
            for cat in sorted(set(theme.flatten())):
                mzones[zone] = mzones.get(zone, (zones == zone))
                mcats[cat] = mcats.get(cat, (theme == cat))
                print(cat, zone, (mzones[zone]  * mcats[cat]).sum())


#if __name__ == "__main__":
#    import doctest
#    doctest.testmod()