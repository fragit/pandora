#! /usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import * 
from PyQt4.QtGui import * 

import os, sys

import string
import csv

import commands

from numpy.ma.core import exp


try:
    import sqlite3
except:
    print "librerie per la connessione al database sqlite non trovate"  


try:
    from osgeo import gdal,ogr
except:
    print "librerie gdal/ogr non trovate"   
    
 
    
from qgis.core import *
from qgis.gui import *
from qgis.analysis import *

import time


class MyMessageBox(QtGui.QWidget):
     def __init__(self, parent=None):
         QtGui.QWidget.__init__(self, parent)
         self.setGeometry(300, 300, 250, 150)
         self.setWindowTitle('message box')

    
    
class UdP:
    def __init__(self,controllo_clima,controllo_aspect,controllo_soil,controllo_patch,path_db,vettoriale,udp,clima,aspect,path_m,path_f):
        self.controllo_clima=controllo_clima
        self.controllo_aspect=controllo_aspect
        self.controllo_patch=controllo_patch
        self.controllo_soil=controllo_soil
        self.path_db=path_db
        self.vettoriale=vettoriale
        self.udp=udp
        self.clima=clima
        self.aspect=aspect
        self.path_m=path_m
        self.path_f=path_f
        
        self.stop_ciclo=False
        
    
    def estrai_clc(self):                       
        self.conn = sqlite3.connect(self.path_db)
        
        self.diz_clc2=dict()
        self.diz_clc3=dict()
        self.diz_clc4=dict()
        self.sql2="select distinct CLC_CODE_2 as clc2, BTC2 as btc from clc order by clc2"
        self.sql3="select distinct CLC_CODE_3 as clc3, BTC3 as btc from clc order by clc3"
        self.sql4="select distinct CLC_CODE_4 as clc4, BTC4 as btc from clc order by clc4"
        self.ris2=self.conn.execute(self.sql2)

        for self.row2 in self.ris2:
            self.nome_udp=self.row2[0]
            self.diz_clc2[self.nome_udp]=self.row2[1]
        self.ris3=self.conn.execute(self.sql3)
        for self.row3 in self.ris3:
            self.nome_udp=self.row3[0]
            self.diz_clc3[self.nome_udp]=self.row3[1]
        self.ris4=self.conn.execute(self.sql4)
        for self.row4 in self.ris4:
            self.nome_udp=self.row4[0]
            self.diz_clc4[self.nome_udp]=self.row4[1]

        return self.diz_clc2,self.diz_clc3,self.diz_clc4
    
    
    def estraivc(self,diz_x_es):
        diz_vc=dict()
        #import ipdb; ipdb.set_trace()
        for key,value in diz_x_es.iteritems():
            clcvalue=int(value)
            if len(str(clcvalue))==4:
                clcfield='CLC_CODE_4' 
            if len(str(clcvalue))==3:
                clcfield='CLC_CODE_3' 
            if len(str(clcvalue))==2:
                clcfield='CLC_CODE_2' 
            if len(str(clcvalue))==1:
                clcfield='CLC_CODE_1'
            sqlstring='select '+str(clcvalue)+' as clcvc, vc as vc from clc where '+clcfield+'="'+str(clcvalue)+'" limit 1'
            risvc=self.conn.execute(sqlstring)
            for rowvc in risvc:
                valorevc=rowvc[1]
            diz_vc[key]=valorevc      
        return diz_vc



    #*****************************************
    #da eliminare
    #temporanea
    
    def calcolareaudp(self):
        iter=self.udp.getFeatures()
        feat = QgsFeature()
        idx_udp = self.udp.fieldNameIndex('belu')
        for feat in iter:
            attrs = feat.attributes()
            codice=attrs[idx_udp]
            self.diz_udp[int(codice)]=feat.geometry().area()
        return self.diz_udp
    #*****************************************
              
    
    def analisiudp(self,diz2,diz3,diz4):        
        self.dizionario2=diz2
        self.dizionario3=diz3
        self.dizionario4=diz4                
        self.result=""
        self.area=0
        self.btc=0
        self.btc2=0
        self.btctot=0
        self.areatot=0.0000
        self.areatot_t=0.0000
        self.i=0
        self.iter=self.feat=None
        
        self.result+="\n---------------------------\n"
        self.result+="\nCheck join between patch and BELU map\n"        

        #print "--------------"
        #print "lista campi"
        self.campi_vettoriale=self.getFieldList(self.vettoriale)
        self.campi_udp=self.getFieldList(self.udp)
        """
        if not self.lista_campi_vettoriale.index('COD') or self.lista_campi_udp.index('udp__'):
            QMessageBox.warning(self.dlg,"Warning", "Shapefile not correct" )
        """    
        #print self.campi_vettoriale.count()   
        self.lista_campi_vettoriale=[] 
        self.lista_campi_udp=[]
        for k in range(self.campi_vettoriale.count()):
            self.lista_campi_vettoriale.append(self.campi_vettoriale[k].name())
        for k in range(self.campi_udp.count()):
            self.lista_campi_udp.append(self.campi_udp[k].name()) 
        try: 
            indexcod=self.lista_campi_vettoriale.index('COD')
            indexudp=self.lista_campi_udp.index('belu')
            pass
        except:
            self.result+="\nWarning: Check shapefile fields\n"
            self.result+="\n---------------------------\n"
            return self.result,0,0,0,0,0,0,0,0                   
        #print self.lista_campi_vettoriale  
        try:
            self.lista_campi_vettoriale.index('belu')
            self.result+="\nField BELU found\n"
            self.result+="\nspatial join not required\n"
            self.result+="\nShapefile analyzed:\n"
            self.result+=self.vettoriale.name()
            self.result+="\n---------------------------\n"
            self.vettore_alfa=self.vettoriale            
            pass    
        except:
            self.result+="\nJoin needed\n"   
            self.intersezione() 
            self.vettore_alfa = QgsVectorLayer("./belu_intersect.shp", "belu_intersect", "ogr")
            self.result+="\nJoin processed\n\n"
            self.result+="\nNew shapefile analyzed:\n"
            self.result+=self.vettore_alfa.name()
            QgsMapLayerRegistry.instance().addMapLayer(self.vettore_alfa)

            self.result+="\n\n---------------------------\n"
            
        self.vettoriale_path=string.split(self.vettore_alfa.dataProvider().dataSourceUri(), "|")
        
        self.idx_udp = self.vettore_alfa.fieldNameIndex('belu') 
        self.idx_cod = self.vettore_alfa.fieldNameIndex('COD')
        self.iter=self.vettore_alfa.getFeatures()
        self.feat = QgsFeature()
        self.i=0
        self.listaudp=[]
        for self.feat in self.iter:
            #metto in una lista e stampo tutti gli attributi
            self.attrs = self.feat.attributes()    
            #print self.attrs                
            self.i+=1
            self.polygon_alfaArea=self.feat.geometry().area() #ottengo l'area della feature
            self.areatot+=self.polygon_alfaArea
            try:
                self.listaudp.index(self.attrs[self.idx_udp])
                exit
            except:
                self.listaudp.append(self.attrs[self.idx_udp])
        self.result+= "BELU analysis\n\n"
        self.result+= "Number of patches : %d\n\n" %self.i
        self.numeroudp=len(self.listaudp)
        self.result+="Number of BELU %d\n\n"%self.numeroudp 
        self.diz_udp=dict()
        self.diz_udp_btc=dict()
        self.diz_perimetritot=dict()
        self.diz_perimetritot0=dict()
        self.diz_areetotali0=dict()
        self.diz_U=dict()

        
        #inizializzo i dizionari
        for k in range(self.numeroudp):    
            self.udp_temp=self.listaudp[k] 
            self.diz_udp[self.udp_temp]=0
            self.diz_udp_btc[self.udp_temp]=0
            self.diz_perimetritot[self.udp_temp]=0
            self.diz_perimetritot0[self.udp_temp]=0
            self.diz_areetotali0[self.udp_temp]=0   
            self.diz_U[self.udp_temp]=0
        #print "stampo il diz_udp"
        #print self.diz_udp  
        
        self.feat=self.iter=self.polygon_alfaArea=self.i=self.attrs=None
        
        self.iter=self.vettore_alfa.getFeatures()
        self.feat = QgsFeature()
        """
        ### test per area 13
        self.btc13=0
        self.btc_13_2=0
        #fine test per area 13
        """
        for self.feat in self.iter:
            self.attrs = self.feat.attributes()
            self.polygon_alfaArea=self.feat.geometry().area()
            self.polygon_alfaPerimetro=self.feat.geometry().length()
            self.nomeudp=self.attrs[self.idx_udp]
            self.codice=self.attrs[self.idx_cod]
            if len(str(int(self.codice)))==4:
                self.diz_clc=self.dizionario4
            if len(str(int(self.codice)))==3:               
                self.diz_clc=self.dizionario3
            if len(str(int(self.codice)))==2:              
                self.diz_clc=self.dizionario2 
            if len(str(int(self.codice)))==1:              
                self.diz_clc=self.dizionario2                

            self.area=self.diz_udp[self.nomeudp]
            self.area=self.area+self.polygon_alfaArea          
            self.diz_udp[self.nomeudp]=self.area 
            
  
         
            
            try:               
                self.btc=self.diz_clc[int(self.codice)]    
                self.btc2=self.btc*self.polygon_alfaArea
                self.diz_udp_btc[self.nomeudp]+=self.btc2
                if float(self.btc)==0.0:       
                    self.diz_perimetritot0[self.nomeudp]+=self.polygon_alfaPerimetro
                    self.area0=self.diz_areetotali0[self.nomeudp]
                    self.area0=self.area0+self.polygon_alfaArea          
                    self.diz_areetotali0[self.nomeudp]=self.area0   
                    
                if float(self.btc)>0:
                    self.perimetro=self.diz_perimetritot[self.nomeudp]
                    self.perimetro=self.perimetro+self.polygon_alfaPerimetro          
                    self.diz_perimetritot[self.nomeudp]=self.perimetro             
                
            except:
                pass
            self.area=0
            self.btc=0
            self.btc2=0
            
        """
        print "test per area 13"
        print self.btc13*self.diz_udp["13"]
        print self.btc_13_2
        print "fine test area 13"
        """
        """
        print "superfici"
        print self.diz_udp
        print "BTC per udp"  
        print self.diz_udp_btc
        """
        
        self.feat=self.iter=self.polygon_alfaArea=self.attrs=None   
        
        for self.x in self.diz_udp.keys():
            self.a_1=self.diz_udp[self.x]
            self.areatot_t=self.areatot_t+self.a_1
            self.a_1=self.a_1/1000000
            self.a=str(self.a_1)  
            self.result+="BELU code %s"%self.x+" "
            self.result+="area: %s"%self.a+" square kilometers\n"        
        self.result+="\nTotal study area: %s"%str(self.areatot/1000000)
        self.result+=" square kilometers\n"
        self.result+="---------------------------\n"
        for self.x in self.diz_udp.keys():
            self.b_1=self.diz_udp_btc[self.x]
            self.btctot=self.btctot+self.b_1
            self.b=str(self.b_1)  
            self.result+="BELU code %s"%self.x+" "
            self.result+="BTC: %s"%self.b+" Mcal/year\n"     
        self.result+="\nTotal BTC detected: %s"%str(self.btctot)
        self.result+=" Mcal/year\n"
        self.result+="---------------------------\n"  
        
        self.x=self.areatot0=self.perimetro=self.perimetro0=None
        
        #print "aree totali con btc 0"
        #print self.diz_areetotali0
      
        for self.x in self.diz_udp.keys():
            self.areatot=self.diz_udp[self.x]
            self.areatot0=self.diz_areetotali0[self.x]        

            self.diz_U[self.x]=self.areatot0/self.areatot

                
        return self.result,self.diz_udp,self.diz_udp_btc,self.vettoriale_path, self.diz_U, self.diz_perimetritot0,self.diz_perimetritot,self.areatot_t,self.numeroudp
    
    
    
    def calcolaKpatch(self,diz_kappapatch,diz_perimetri_tot,diz_perimetri): 
        
        result_analizzapatch="\nKAPPA PATCH ANALYSIS\n\n"
        
        for key,value in diz_perimetri.iteritems():
            result_analizzapatch+="BELU code %s " %str(key)
            valore_patch=(1-(value/diz_perimetri_tot[key]))
            if valore_patch<0:
                valore_patch=0

            diz_kappapatch[key]=valore_patch
            result_analizzapatch+="kappa = %s" %str(diz_kappapatch[key])
            result_analizzapatch+="\n"
            
        result_analizzapatch+="\n---------------------------\n"
        #print "dizionario kappa patch"
        #print diz_kappapatch
        return result_analizzapatch,diz_kappapatch
                        
        
        
    def calcolah(self,diz_h,diz_perimetri,diz_perimetritot0):
        self.diz_h=diz_h
        self.diz_perimetri=diz_perimetri
        self.diz_perimetritot0=diz_perimetritot0
        
        self.x=self.value=None
        
        for self.x,self.value in self.diz_perimetri.iteritems():
            self.diz_h[int(self.x)]=self.diz_perimetritot0[int(self.x)]/self.value    
        
        return self.diz_h    
        

        
    def inizializzadiz(self,dizionario):
        self.diz_temp=dizionario
        self.x=0
        for self.x in self.diz_udp.keys():
            self.diz_temp[int(self.x)]=0
        return self.diz_temp
        
    
    # Return the field list of a vector layer
    def getFieldList(self,vlayer ):
        #print vlayer.displayField()
        vprovider = vlayer.dataProvider()
        myFields = vprovider.fields()
        return myFields    
    
    
    def analisiKJ(self,cont_costanti,diz_udp_clima,diz_udp_esp,diz_udp_patch,diz_udp_soil):        
        self.cont_costanti=cont_costanti
        self.diz_udp_clima=diz_udp_clima
        self.diz_udp_esp=diz_udp_esp
        self.diz_udp_patch=diz_udp_patch
        self.diz_udp_soil=diz_udp_soil
        
        self.diz_kappaj=dict()
        self.diz_kappaj=self.inizializzadiz(self.diz_kappaj)
        
        #print "controllo costanti"
        #print self.cont_costanti
        
        result_kjmedio="\nAVERAGE KAPPA ANALYSIS\n\n"
        
        
        for self.nome_udp in self.diz_udp_clima.keys():
            self.Kj=(self.diz_udp_clima[int(self.nome_udp)]+self.diz_udp_esp[int(self.nome_udp)]+self.diz_udp_patch[int(self.nome_udp)]+self.diz_udp_soil[int(self.nome_udp)])
            self.Kj=self.Kj/self.cont_costanti
            result_kjmedio+="BELU code %s " %str(self.nome_udp)
            result_kjmedio+="average kappa = %s" %str(self.Kj)
            result_kjmedio+="\n"
            self.diz_kappaj[int(self.nome_udp)]=self.Kj
        
        #da eliminare
        #self.diz_kappaj={1: 0.856528, 2: 0.829098, 3: 0.813867, 4: 0.783433, 5: 0.791109, 6: 0.809587, 7: 0.542433}
        #self.diz_kappaj={1: 1, 2: 1, 3: 1, 4: 1, 5: 0, 6: 0.001, 7: 0.542433}
        #self.diz_kappaj={1: 1, 2: 0.829098, 3: 0.813867, 4: 0.783433, 5: 0.791109, 6: 0.809587, 7: 0.542433}
        result_kjmedio+="\n---------------------------\n"
        
        return self.diz_kappaj,result_kjmedio
        
        
                
    
    def intersezione(self):     
        #layer vettoriale e layer udp: self.vettoriale self.udp
        self.outdriver = ogr.GetDriverByName("ESRI Shapefile")
        self.outpath_intersect="./belu_intersect.shp"
        self.overlayAnalyzer = QgsOverlayAnalyzer() 
        
        if os.path.exists(self.outpath_intersect):
            self.outdriver.DeleteDataSource(self.outpath_intersect)  
            
        self.overlayAnalyzer.intersection(self.vettoriale, self.udp, self.outpath_intersect)  
        #self.overlayAnalyzer.intersection(self.vettoriale, self.udp, r"./shape_intersect2.shp")
        
    def risultatoM(self,diz_kappaj,diz_udp_btc):
        self.diz_udp_btc=diz_udp_btc
        self.diz_kappaj=diz_kappaj
        self.resultM="LANDSCAPE AVAILABLE ENERGY\n\n"
        
        self.diz_udp_M=dict()
        self.diz_udp_Mmax=dict()
        self.area=0
        
        for self.nome_udp in self.diz_udp_btc.keys():
            self.Bj=self.diz_udp_btc[self.nome_udp]
            #self.Kj=self.diz_udp_clima[int(self.nome_udp)]
            self.Kj=self.diz_kappaj[int(self.nome_udp)]          
            self.Mj=(self.Kj+1)*self.Bj
            self.area=float(self.diz_udp[self.nome_udp])
            self.Mmax=2*6.5*self.area
            self.diz_udp_M[int(self.nome_udp)]=self.Mj
            self.diz_udp_Mmax[int(self.nome_udp)]=self.Mmax
            self.resultM+="Landscape unit code %s " %self.nome_udp
            self.resultM+="M = %s" %self.Mj 
            self.resultM+="\n"     
        
        self.resultM+="\n---------------------------\n"            
        return self.diz_udp_M,self.resultM,self.diz_udp_Mmax
            

    def calcola_flusso(self,diz_udp_M,diz_bordi,diz_permeability,diz_perimetri,diz_udp_Mmax):        
        self.diz_udp_M=diz_udp_M
        self.diz_bordi=diz_bordi
        self.diz_permeability=diz_permeability
        self.diz_perimetri=diz_perimetri
        self.diz_udp_Mmax=diz_udp_Mmax
        self.diz_F=dict()
        self.diz_Fmax=dict()
        self.resultF="\nLANDSCAPE ENERGY FLOW\n\n"
        for self.udp_key,self.udp_value in self.diz_bordi.iteritems():
            self.lista_coppia=string.split(self.udp_key, "-")
            self.udp_prima=self.lista_coppia[0]
            self.udp_seconda=self.lista_coppia[1]    
            self.udp_prima_M=self.diz_udp_M[int(self.udp_prima)]
            self.udp_seconda_M=self.diz_udp_M[int(self.udp_seconda)]  
            self.udp_prima_Mmax=self.diz_udp_Mmax[int(self.udp_prima)]
            self.udp_seconda_Mmax=self.diz_udp_Mmax[int(self.udp_seconda)]                   
            self.udp_prima_P=self.diz_perimetri[int(self.udp_prima)]
            self.udp_seconda_P=self.diz_perimetri[int(self.udp_seconda)]      
            #qui di seguito viene preso in considerazione solo una barriera tra le udp
            #nel caso di più barriere va ciclato
            self.udp_L=self.udp_value
            self.udo_p=float(self.diz_permeability[self.udp_key])
            self.flusso=(self.udp_prima_M+self.udp_seconda_M)/(2*(self.udp_prima_P+self.udp_seconda_P))*self.udp_L*self.udo_p
            self.flussomax=(self.udp_prima_Mmax+self.udp_seconda_Mmax)/(2*(self.udp_prima_P+self.udp_seconda_P))*self.udp_L
            self.diz_F[self.udp_key]=self.flusso
            self.diz_Fmax[self.udp_key]=self.flussomax
            
            self.resultF+="BELU neighboring %s " %self.udp_key
            self.resultF+="F = %s" %self.flusso
            self.resultF+="\n"
            
        self.resultF+="\n---------------------------\n"    
        
        return self.diz_F,self.resultF,self.diz_Fmax
    
    
    def sommaF(self,diz_sommaF,diz_sommaFmax,diz_flussi,diz_flussi_max):  
        self.diz_sommaF=diz_sommaF
        self.diz_sommaFmax=diz_sommaFmax   
        self.diz_flussi=diz_flussi
        self.diz_flussi_max=diz_flussi_max
        self.udp_key=self.udp_value=None
        for self.udp_key,self.udp_value in self.diz_flussi.iteritems():
            self.lista_coppia=string.split(self.udp_key, "-")
            self.udp_prima=self.lista_coppia[0]
            self.udp_seconda=self.lista_coppia[1] 
            self.diz_sommaF[int(self.udp_prima)]+=self.udp_value
            self.diz_sommaF[int(self.udp_seconda)]+=self.udp_value 
        self.udp_key=self.udp_value=None  
        for self.udp_key,self.udp_value in self.diz_flussi_max.iteritems():
            self.lista_coppia=string.split(self.udp_key, "-")
            self.udp_prima=self.lista_coppia[0]
            self.udp_seconda=self.lista_coppia[1] 
            self.diz_sommaFmax[int(self.udp_prima)]+=self.udp_value
            self.diz_sommaFmax[int(self.udp_seconda)]+=self.udp_value    
            
        return self.diz_sommaF,self.diz_sommaFmax

    def costanteC(self,diz_sommaF,diz_sommaFmax):
        self.diz_sommaF=diz_sommaF
        self.diz_sommaFmax=diz_sommaFmax 
        self.diz_C=dict() 
        self.nome_udp=None
        for self.nome_udp in self.diz_sommaF.keys():
            self.diz_C[int(self.nome_udp)]=self.diz_sommaF[int(self.nome_udp)]/self.diz_sommaFmax[int(self.nome_udp)]
        return self.diz_C        
        
    
        
    def outputM(self,outshape):
        self.udp_path=string.split(self.udp.dataProvider().dataSourceUri(), "|")
        
        self.inshape=self.udp_path[0]
        self.outshape=outshape
        
        self.diz_centroidi=dict()

        #leggo il mio shape in entrata
        self.inshape_driver=ogr.GetDriverByName("ESRI Shapefile")
        self.inshape_datasource=self.inshape_driver.Open(self.inshape, 0)        
        self.inlayer=self.inshape_datasource.GetLayer()
        
        #preparo lo shape in uscita
        self.outdriver = ogr.GetDriverByName("ESRI Shapefile")
        
        # Rimuovo lo shape out se c'è già
        if os.path.exists(self.outshape):
            self.outdriver.DeleteDataSource(self.outshape)
        #creo il mio shape di uscita
        self.outshape_datasource=self.outdriver.CreateDataSource(self.outshape)
        self.outlayer=self.outshape_datasource.CreateLayer("belu_centroids", geom_type=ogr.wkbPoint)
        
        #aggiungo i campi
        self.new_udp = ogr.FieldDefn('belu', ogr.OFTInteger)
        self.new_M = ogr.FieldDefn('M', ogr.OFTReal)        
        self.outlayer.CreateField(self.new_udp)
        self.outlayer.CreateField(self.new_M)
        
        self.outlayerdefn = self.outlayer.GetLayerDefn()
        
        for self.j in range(0,self.inlayer.GetFeatureCount()):
            self.infeature = self.inlayer.GetFeature(self.j)
            self.outfeature = ogr.Feature(self.outlayerdefn)
            self.inlayerudp=self.infeature.GetField("belu")
            self.outfeature.SetField("belu",self.inlayerudp)
            self.outlayerM=self.diz_udp_M[int(self.inlayerudp)]
            self.outfeature.SetField("M",self.outlayerM)
            self.infeaturegeom = self.infeature.GetGeometryRef()
            self.centroid = self.infeaturegeom.Centroid()
            self.outfeature.SetGeometry(self.centroid)
            self.outlayer.CreateFeature(self.outfeature)
            
            self.diz_centroidi[int(self.inlayerudp)]=self.centroid.ExportToWkt()
        
        self.outshape_datasource.Destroy()
        self.inshape_datasource.Destroy()
        self.result_buildoutM="\nNow Pandora 3.0 can be implemented\n"
        self.path_nuovofile=os.path.split(self.outshape)        
        try:
            self.nuovonome=string.split(self.path_nuovofile[1], ".")
            self.nuovonome=self.nuovonome[0]
        except:
            self.nuovonome=self.path_nuovofile[1]
        self.output_vlayer = QgsVectorLayer(self.outshape, self.nuovonome, "ogr")
        return self.result_buildoutM,self.diz_centroidi,self.output_vlayer
    
    
    def modifyM(self,outshape,dizionario,ciclo,campo1,campo2):
        provider = outshape.dataProvider()
        iter=outshape.getFeatures()
        idx_udp = outshape.fieldNameIndex(campo1) 
        caps = provider.capabilities()
        if caps & QgsVectorDataProvider.AddAttributes:
            provider.addAttributes( [ QgsField(campo2, QVariant.Double) ] )
        outshape.updateFields()
        idx_cod = outshape.fieldNameIndex(campo2)      
        feat = QgsFeature()
        outshape.startEditing()

        for feat in iter:
            attrs = feat.attributes()
            nomeudp=attrs[idx_udp]
            value=str(dizionario[int(nomeudp)][ciclo-1])            
            pippo=outshape.changeAttributeValue(feat.id(), idx_cod, value)
        outshape.commitChanges()
        outshape.updateFields()

    
    def outputF(self,diz_flussi,diz_centroidi,outshapeF):
        self.outshapeF=outshapeF
        self.diz_flussi=diz_flussi
        self.diz_centroidi=diz_centroidi
                          
        self.driver=ogr.GetDriverByName('ESRI Shapefile')
        #self.outshapeF="F"

        
        # Rimuovo lo shape out se c'è già
        if os.path.exists(self.outshapeF):
            self.driver.DeleteDataSource(self.outshapeF)
                    
        self.dsF=self.driver.CreateDataSource(self.outshapeF)
        self.layerF=self.dsF.CreateLayer('flows', geom_type=ogr.wkbLineString)
        
        self.fieldDefnudp=ogr.FieldDefn('belu', ogr.OFTString)
        self.fieldDefnF=ogr.FieldDefn('Fvalue', ogr.OFTReal)
        self.layerF.CreateField(self.fieldDefnudp)
        self.layerF.CreateField(self.fieldDefnF)
        self.layerFDefn=self.layerF.GetLayerDefn()
        
        for self.udp_key,self.udp_value in self.diz_flussi.iteritems():
            #estraggo gli elementi dal dizionario delle coppie
            self.lista_coppia=string.split(self.udp_key, "-")
            self.udp_prima=self.lista_coppia[0]
            self.udp_seconda=self.lista_coppia[1]
            #creo la mia feature
            self.featureF=ogr.Feature(self.layerFDefn)
            
            self.point_udp_prima=ogr.CreateGeometryFromWkt(self.diz_centroidi[int(self.udp_prima)])
            self.point_udp_seconda=ogr.CreateGeometryFromWkt(self.diz_centroidi[int(self.udp_seconda)])
            self.linea=ogr.Geometry(ogr.wkbLineString)
            self.linea.AddPoint(self.point_udp_prima.GetX(),self.point_udp_prima.GetY())
            self.linea.AddPoint(self.point_udp_seconda.GetX(),self.point_udp_seconda.GetY())
            
            self.featureF = ogr.Feature(self.layerFDefn)            
            self.featureF.SetField('belu',self.udp_key)
            self.featureF.SetField('Fvalue',self.udp_value)
            self.featureF.SetGeometry(self.linea)
            self.layerF.CreateFeature(self.featureF)  
            self.featureF.Destroy()
            self.linea.Destroy()
              
        self.dsF.Destroy()
        
        self.path_nuovofile=os.path.split(self.outshapeF)
        #self.vettoriale_path=string.split(self.vettore_alfa.dataProvider().dataSourceUri(), "|")
        try:
            self.nuovonome=string.split(self.path_nuovofile[1], ".")
            self.nuovonome=self.nuovonome[0]
        except:
            self.nuovonome=self.path_nuovofile[1]
        self.output_vlayerF = QgsVectorLayer(self.outshapeF, self.nuovonome, "ogr")

        
        return self.output_vlayerF  
    

    def modelloK(self,ci,hi,ui,bi,costanteA,costanteB):
        self.ci=ci
        self.hi=hi
        self.ui=ui
        self.bi=bi
        
        #self.k0=(self.ci-(self.hi*self.ui)-self.ci*self.bi)/self.bi
        
        try:
            self.k0=(self.ci-((costanteA*self.hi)*(costanteB*self.ui))-self.ci*self.bi)/self.bi
        except:
            self.k0=0
        
        return self.k0
    
    def modelloB(self,k,c,h,u,costanteA,costanteB):
        self.k=k
        self.c=c
        self.h=h
        self.u=u
        
        self.var1=(-(self.c-(costanteA*self.h)*(costanteB*self.u)))
        self.potenza=exp(self.var1)

        """
        if self.k==0:
            self.B=0
        else:
            self.B=(self.c-(self.h*self.u))/(self.k**(-(self.c-self.h*self.u))+self.c)
        """
        self.B=(self.c-((costanteA*self.h)*(costanteB*self.u)))/(self.k*self.potenza+self.c)
        
        return self.B    
    
    def stop_dm(self):
        self.stop_ciclo=True
    
    def flussoModello(self,diz_udp_M,diz_udp_Mmax,diz_bordi,diz_permeability,diz_perimetri,ciclo):        
        self.diz_udp_M=diz_udp_M
        self.diz_bordi=diz_bordi
        self.diz_permeability=diz_permeability
        self.diz_perimetri=diz_perimetri
        self.diz_udp_Mmax=diz_udp_Mmax
        self.diz_F2=dict()
        self.diz_F2max=dict()
        self.diz_sommaF=dict()
        self.diz_sommaFmax=dict()
        self.ciclo=ciclo
        
        for self.udp_key,self.udp_value in self.diz_bordi.iteritems():
            self.lista_coppia=string.split(self.udp_key, "-")
            self.udp_prima=self.lista_coppia[0]
            self.udp_seconda=self.lista_coppia[1]    
            self.udp_prima_M=self.diz_udp_M[int(self.udp_prima)][self.ciclo]
            self.udp_seconda_M=self.diz_udp_M[int(self.udp_seconda)][self.ciclo]  
            self.udp_prima_Mmax=self.diz_udp_Mmax[int(self.udp_prima)]
            self.udp_seconda_Mmax=self.diz_udp_Mmax[int(self.udp_seconda)]                   
            self.udp_prima_P=self.diz_perimetri[int(self.udp_prima)]
            self.udp_seconda_P=self.diz_perimetri[int(self.udp_seconda)]      
            #qui di seguito viene preso in considerazione solo una barriera tra le udp
            #nel caso di più barriere va ciclato
            self.udp_L=self.udp_value
            self.udo_p=float(self.diz_permeability[self.udp_key])
            self.flusso=(self.udp_prima_M+self.udp_seconda_M)/(2*(self.udp_prima_P+self.udp_seconda_P))*self.udp_L*self.udo_p
            self.flussomax=(self.udp_prima_Mmax+self.udp_seconda_Mmax)/(2*(self.udp_prima_P+self.udp_seconda_P))*self.udp_L
            self.diz_F2[self.udp_key]=self.flusso
            self.diz_F2max[self.udp_key]=self.flussomax
        
        self.diz_sommaF=self.inizializzadiz(self.diz_sommaF)
        self.diz_sommaFmax=self.inizializzadiz(self.diz_sommaFmax)
        
        self.udp_key=self.udp_value=None
        for self.udp_key,self.udp_value in self.diz_F2.iteritems():
            self.lista_coppia=string.split(self.udp_key, "-")
            self.udp_prima=self.lista_coppia[0]
            self.udp_seconda=self.lista_coppia[1] 
            self.diz_sommaF[int(self.udp_prima)]+=self.udp_value
            self.diz_sommaF[int(self.udp_seconda)]+=self.udp_value 
        self.udp_key=self.udp_value=None  
        for self.udp_key,self.udp_value in self.diz_F2max.iteritems():
            self.lista_coppia=string.split(self.udp_key, "-")
            self.udp_prima=self.lista_coppia[0]
            self.udp_seconda=self.lista_coppia[1] 
            self.diz_sommaFmax[int(self.udp_prima)]+=self.udp_value
            self.diz_sommaFmax[int(self.udp_seconda)]+=self.udp_value   
            
        self.diz_C2=dict() 
        self.nome_udp=None
        for self.nome_udp in self.diz_sommaF.keys():
            self.diz_C2[int(self.nome_udp)]=self.diz_sommaF[int(self.nome_udp)]/self.diz_sommaFmax[int(self.nome_udp)]              

        return self.diz_C2   

    
    def inizioModello(self,diz_h,diz_U,diz_C,diz_kappaj,diz_bordi,diz_permeability,diz_perimetri,costanteA,costanteB):
        self.diz_C=diz_C
        self.diz_U=diz_U
        self.diz_h=diz_h       
        self.diz_kappaj=diz_kappaj
        
        listatemp=[]
        
        self.diz_bordi=diz_bordi
        self.diz_permeability=diz_permeability
        self.diz_perimetri=diz_perimetri
        
        self.feat=self.iter=self.i=self.attrs=self.nomeudp=self.geom_id=self.geom=self.codice=None
        self.idx_udp = self.vettore_alfa.fieldNameIndex('belu') 
        self.idx_cod = self.vettore_alfa.fieldNameIndex('COD')
        self.iter=self.vettore_alfa.getFeatures()
        self.feat = QgsFeature()
        
        self.diz_patch_udp=dict()
        self.diz_patch_area=dict()
        self.diz_patch_clc=dict()
        self.diz_patch_B=dict()
        self.diz_patch_K=dict()
        self.diz_patch_M=dict()
        self.diz_patch_Mmax=dict()
        self.diz_udp_M2=dict()
        self.diz_udp_M2max=dict()
        self.diz_udp_C=dict()
        
        self.diz_udp_M2=self.inizializzadiz(self.diz_udp_M2)
        self.diz_udp_M2max=self.inizializzadiz(self.diz_udp_M2max)
        self.diz_udp_C=self.inizializzadiz(self.diz_udp_C)
        
        self.diz_clc=dict()

        #print "inizio modello"
        
        for self.feat in self.iter: 

            self.listabtc=[]
            self.listaK=[]
            self.attrs = self.feat.attributes()
            self.nomeudp=self.attrs[self.idx_udp]
            self.codice=self.attrs[self.idx_cod]
            self.geom=self.feat.geometry()
            self.geom_id=self.feat.id()            
            self.area=self.geom.area()
            
            
            if len(str(int(self.codice)))==4:
                self.diz_clc=self.dizionario4
            if len(str(int(self.codice)))==3:
                self.diz_clc=self.dizionario3
            if len(str(int(self.codice)))==2:
                self.diz_clc=self.dizionario2
            if len(str(int(self.codice)))==1:
                self.diz_clc=self.dizionario2                   
                
            
                  
            
            self.diz_patch_udp[self.geom_id]=self.nomeudp
            self.diz_patch_area[self.geom_id]=self.area
            self.diz_patch_clc[self.geom_id]=self.codice
            
            try:              
                
                self.btc=self.diz_clc[int(self.codice)]
                self.B=self.btc/6.5

                self.listabtc.append(self.B)                
                self.diz_patch_B[self.geom_id]=self.listabtc
            except:
                self.btc=0
                self.B=0
                self.listabtc.append(self.B)                
                self.diz_patch_B[self.geom_id]=self.listabtc                                        
            
            self.ci=self.diz_C[int(self.nomeudp)]
            self.U=self.diz_U[int(self.nomeudp)]
            self.h=self.diz_h[int(self.nomeudp)]
            
            if self.B==0:
                self.K0=0
            else:
                self.K0=self.modelloK(self.ci, self.h, self.U, self.B,costanteA,costanteB)
            self.listaK.append(self.K0)
            self.diz_patch_K[self.geom_id]=self.listaK 
            
            self.Kj=self.diz_kappaj[int(self.nomeudp)]
            self.M=((self.B*6.5)*self.area)*(1+self.Kj)       
            self.diz_udp_M2[int(self.nomeudp)]+=self.M
            
            
            listatemp_temp=[]            
            
            self.polygon_alfaPerimetro=self.feat.geometry().length()
            
            
            """
            if int(self.nomeudp)==7:
                listatemp_temp.append(str(self.geom_id))
                listatemp_temp.append(",")
                listatemp_temp.append(str(self.btc))
                listatemp_temp.append(",")
                listatemp_temp.append(str(self.codice))
                listatemp_temp.append(",")
                listatemp_temp.append(str(self.area))
                listatemp_temp.append(",")
                listatemp_temp.append(str(self.polygon_alfaPerimetro))
                self.stringlist=''.join(listatemp_temp)
                listatemp.append(self.stringlist) 
          
            """
                        
            self.Mmax=self.area*2*6.5
            self.diz_udp_M2max[int(self.nomeudp)]+=self.Mmax            
            
            self.B=self.btc=0          

        self.listaM2=[]
        
        for self.key in self.diz_udp_M2.keys():                
            self.listaM2.append(self.diz_udp_M2[self.key])
            self.diz_udp_M2[self.key]=self.listaM2
            self.listaM2=[]
                  
            
        self.feat=self.M=self.iter=self.i=self.attrs=self.nomeudp=self.geom_id=self.geom=self.btc=self.B=self.key=None
        
        self.diz_Cs=self.flussoModello(self.diz_udp_M2, self.diz_udp_Mmax, self.diz_bordi, self.diz_permeability, self.diz_perimetri,0)
        
        
        self.listaC=[] 
        
        self.key=self.value=None
        
        for self.key,self.value in self.diz_Cs.iteritems():                
            self.listaC.append(self.value)
            self.diz_udp_C[self.key]=self.listaC
            self.listaC=[]  
        
        """
        self.file=open("/home/francesco/lista.csv", 'wb')
        self.writer=csv.writer(self.file, delimiter='|')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in listatemp:  
            self.writer.writerow([self.item]) 
        """       

        return self.diz_patch_udp,self.diz_patch_area,self.diz_patch_B,self.diz_patch_K,self.diz_patch_M,self.diz_patch_Mmax,self.diz_udp_M2,self.diz_udp_M2max,self.diz_udp_C,self.diz_patch_clc
        
        
       
    
    
    def cicloModello(self,diz_kappaj,diz_patch_udp,diz_patch_area,diz_patch_B,diz_patch_K,diz_patch_M,diz_patch_Mmax,diz_udp_M2,diz_udp_M2max,diz_udp_C,diz_U,diz_h,cicli,dlg,step,costanteA,costanteB):
        self.diz_kappaj=diz_kappaj  
        self.diz_patch_udp=diz_patch_udp
        self.diz_patch_area=diz_patch_area
        self.diz_patch_B=diz_patch_B
        self.diz_patch_K=diz_patch_K
        self.diz_patch_M=diz_patch_M
        self.diz_patch_Mmax=diz_patch_Mmax
        self.diz_udp_M2=diz_udp_M2
        self.diz_udp_M2max=diz_udp_M2max
        self.diz_udp_C=diz_udp_C
        self.diz_U=diz_U
        self.diz_h=diz_h              
        self.cicli=cicli
        self.dlg=dlg   
          

        self.listaB=[]
        self.listaK=[]
        
        #print self.diz_patch_B
        
        if step=="mod":
            progressbar=self.dlg.progressBar
        elif step=="dm":
            progressbar=self.dlg.progressBar_dm1
        
        #print "ciclo Modello"
        progressbar.setValue(0)
        progressbar.setMaximum(self.cicli-1)
        
        
        for self.i in range(1,self.cicli):
            
            progressbar.setValue(self.i)
            
            
            self.diz_udp_Mtemp=dict()          
            self.diz_udp_Mtemp=self.inizializzadiz(self.diz_udp_Mtemp)

            
            self.diz_Cs=self.flussoModello(self.diz_udp_M, self.diz_udp_Mmax, self.diz_bordi, self.diz_permeability, self.diz_perimetri,self.i-1)
            
            self.listaC=[] 
            
            self.key=self.value=None
            
            for self.key,self.value in self.diz_Cs.iteritems():
                self.listaC=self.diz_udp_C[self.key]                
                self.listaC.append(self.value)
                self.diz_udp_C[self.key]=self.listaC
                self.listaC=[]              
                       
            for self.keypatch,self.keyvalue in self.diz_patch_udp.iteritems():
                self.cvalue=self.diz_udp_C[int(self.keyvalue)][self.i-1]
                self.hvalue=self.diz_h[int(self.keyvalue)]
                self.uvalue=self.diz_U[self.keyvalue]
                
                ####################################
                #modifica effettuata per ecosystem   
                if diz_patch_B[self.keypatch][self.i-1]==0.0:
                    self.Kvalue=0
                else:        
                    self.Kvalue=self.diz_patch_K[self.keypatch][self.i-1]
                #se non funziona togliere l'if e mettere solo questa riga sopra
                ####################################
                    
                self.cvalue1=self.diz_udp_C[int(self.keyvalue)][self.i]
                #print self.diz_patch_K[self.keypatch]  
                """            
                if step=="dm":
                    
                    print self.diz_patch_B[self.keypatch][self.i-1]
                    print self.Kvalue
                    raw_input("attesa")
                """
                #calcolo B
                if self.diz_patch_B[self.keypatch][self.i-1]==0:
                    self.Bvalue=0
                else:
                    self.Bvalue=self.modelloB(self.Kvalue, self.cvalue, self.hvalue, self.uvalue,costanteA,costanteB)
                self.listaB=self.diz_patch_B[self.keypatch]
                self.listaB.append(self.Bvalue)
                self.diz_patch_B[self.keypatch]=self.listaB                
                             
                #calcolo K
                if self.Bvalue==0:
                    self.K0=0
                else:                    
                    self.K0=self.modelloK(self.cvalue1, self.hvalue, self.uvalue, self.Bvalue,costanteA,costanteB)
                self.listaK=self.diz_patch_K[self.keypatch]   
                self.listaK.append(self.K0)      
                self.diz_patch_K[self.keypatch]=self.listaK
               
                
                #calcolo i valori di M e Mmax per patch e li metto in un dizionario temporaneo
                self.area=self.diz_patch_area[self.keypatch]
                self.Kj=self.diz_kappaj[int(self.keyvalue)]
                self.M=((self.Bvalue*6.5)*self.area)*(1+self.Kj)                
                self.diz_udp_Mtemp[int(self.keyvalue)]+=self.M 
                
                #area test
                """
                if int(self.keyvalue)==2 and self.keypatch==1070:
                    print "area"
                    print self.area
                    print "k-1"
                    print self.Kvalue
                    print "c,h,u"
                    print self.cvalue,self.hvalue,self.uvalue
                    print "B"
                    print self.Bvalue
                    print "K"
                    print self.K0
                    print "C"
                    print self.diz_udp_C
                    print  
                    raw_input("vai avanti ")                     
                """    
                  
                
            #usando delle liste di appoggio aggiorno i valori dei dizionari di M    
            self.listaM2=[]
 
            for self.key in self.diz_udp_Mtemp.keys():
                self.listaM2=self.diz_udp_M2[self.key]
                self.listaM2.append(self.diz_udp_Mtemp[self.key])               
                self.diz_udp_M2[self.key]=self.listaM2
                self.listaM2=[]
        
        somma=0
        for ch,val in self.diz_udp_M2.iteritems():
            somma+=val[self.cicli-1]


        
        
            
        return somma,self.diz_udp_M2,self.diz_udp_C, self.diz_patch_B     
    
    
    def salvamodello(self,numerocicli,diz_udp_M,diz_M_max,path_to_csv):
        self.cicli=numerocicli
        self.diz_finale=diz_udp_M
        self.path_to_csv=path_to_csv
        
        self.listaudp=[]
        self.listavalori=[]
        self.listapp=[]
        
        self.listapp.append("belu")
        self.listapp.append(",")
        self.listapp.append("max")
        self.listapp.append(",")        
        for self.i in range(0,self.cicli):
            self.listapp.append(str(self.i))
            if self.i==self.cicli:
                pass
            else:
                self.listapp.append(",")
        self.stringlist=''.join(self.listapp)
        self.listaudp.append(self.stringlist)  
        self.listapp=[]        
        
        self.key=self.value=self.i=self.stringlist=self.writer=self.item=None
        
        for self.key,self.value in self.diz_finale.iteritems():
            self.listapp.append(str(self.key))
            self.listapp.append(",")
            self.listapp.append(str(diz_M_max[self.key]))
            self.listapp.append(",")
            for self.i in range(0,self.cicli):
                self.valore=self.value[self.i]
                self.listapp.append(str(self.valore))
                if self.i==self.cicli:
                    pass
                else:
                    self.listapp.append(",")
            self.stringlist=''.join(self.listapp)
            self.listaudp.append(self.stringlist)  
            self.listapp=[]

        self.filef=open(self.path_to_csv, 'wb')
        self.writer=csv.writer(self.filef, delimiter='|')
        for self.item in self.listaudp:  
            self.writer.writerow([self.item]) 
        self.filef.close() 
    
    
    def salvapatchmodello(self,cicli,patch_udp,patch_B,patch_area,path_to_table):
        
        lista=[]
        listapatch=[]
        
        lista.append("cod_patch")
        lista.append(",")
        lista.append("belu")
        lista.append(",")
        lista.append("area")
        lista.append(",")
        for x in range(0,cicli):
            lista.append(str(x))
            if x==cicli:
                pass
            else:
                lista.append(",")  
        stringlist=''.join(lista)  
        listapatch.append(stringlist)    
        
        x=valore=stringlist=None
        lista=[]
        
        
        for key,value in patch_B.iteritems():
            lista.append(str(key))
            lista.append(",")
            lista.append(str(patch_udp[key]))
            lista.append(",")
            lista.append(str(patch_area[key]))
            lista.append(",")
            for x in range(0,cicli):
                #valore=value[x]*patch_area[key]
                valore=value[x]
                lista.append(str(valore))
                if x==cicli:
                    pass
                else:
                    lista.append(",")
            stringlist=''.join(lista)
            listapatch.append(stringlist)
            lista=[]
        
        filef=open(path_to_table, 'wb')
        writer=csv.writer(filef, delimiter='|')
        for item in listapatch:  
            writer.writerow([item]) 
        filef.close()
        
        
        
    def aggiornapatch(self,ciclo,diz_patch_B,diz_patch_area):
        
        outshape=self.vettore_alfa
        provider = outshape.dataProvider()
        iter=outshape.getFeatures()
        caps = provider.capabilities()
        
        if caps & QgsVectorDataProvider.AddAttributes:
            provider.addAttributes( [ QgsField("idpatch", QVariant.Int) ] )
            provider.addAttributes( [ QgsField("area", QVariant.Double) ] )   
            provider.addAttributes( [ QgsField("perimeter", QVariant.Double) ] )
            provider.addAttributes( [ QgsField("BTC", QVariant.Double) ] )
            provider.addAttributes( [ QgsField("Bfinal", QVariant.Double) ] ) 
            provider.addAttributes( [ QgsField("Bfinalst", QVariant.Double) ] ) 
            
        outshape.updateFields()
        idx_id = outshape.fieldNameIndex("idpatch")
        idx_area = outshape.fieldNameIndex("area")
        idx_p = outshape.fieldNameIndex("perimeter")
        idx_btc = outshape.fieldNameIndex("BTC")
        idx_bf = outshape.fieldNameIndex("Bfinal")
        idx_bfs = outshape.fieldNameIndex("Bfinalst")
        idx_cod = outshape.fieldNameIndex("COD")  
        
        iter=outshape.getFeatures()
        feat = QgsFeature()

        outshape.startEditing()
        
        for feat in iter:
            
            idx_id = outshape.fieldNameIndex("idpatch")
            idx_bf = outshape.fieldNameIndex("Bfinal")
            idpatch=int(feat.id())
            area=diz_patch_area[idpatch]
            perimetro=feat.geometry().length()
            cc=int(ciclo)-1
            bfinal=float(diz_patch_B[idpatch][cc])
            bstart=diz_patch_B[idpatch][0]*6.5
            bfs=bfinal*6.5
            
            outshape.changeAttributeValue(idpatch, idx_id, idpatch) 
            outshape.changeAttributeValue(idpatch, idx_area, area)
            outshape.changeAttributeValue(feat.id(), idx_p, perimetro)
            outshape.changeAttributeValue(idpatch, idx_btc, bstart)
            outshape.changeAttributeValue(idpatch, idx_bf, bfinal)
            outshape.changeAttributeValue(feat.id(), idx_bfs, bfs)
            
            #bstart=bfinal=bfs=perimetro=area=idpatch=None     
             
            
        outshape.commitChanges()
        outshape.updateFields()


            
    
          
                           
                    
                
                
            
                                 

                
        
        
                
            
                
        
                                
                    
            
        
           