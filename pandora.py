# -*- coding: utf-8 -*-
"""
/***************************************************************************
 pandora
                                 A QGIS plugin
 Pandora
                              -------------------
        begin                : 2014-01-07
        copyright            : (C) 2014 by Università della Tusca
        email                : geri.francesco@zoho.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from pandoradialog import pandoraDialog
from configurationdialog import *
import os
import os.path
from os.path import isfile, getsize
import sys
import time
import string
import csv
import random
from pylab import *
from copy import deepcopy
import signal
import platform
import operator



from analysis import *

from raster_analysis import *

#from permeability import *

from tabledialog import *

from reclassdialog import *
from csv import Dialect

try:
    import sqlite3
except:
    print "librerie per la connessione al database sqlite non trovate"


class pandora:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        self.canvas=self.iface.mapCanvas()
        self.msgBar = self.iface.messageBar()

        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n', 'pandora_{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)
        
        
                
        # Create the dialog (after translation) and keep reference
        self.dlg = pandoraDialog()
        self.dlg_conf=configurationDialog()        
        
        self.dlg_reclass=reclassDialog()
        
        self.dlg_conf.pathtodb.setText(os.path.dirname(__file__)+"/pandora.sqlite")
        
        self.dlg_conf.path_to_kmean.setEnabled(False)
        self.dlg_conf.button_load_kmean.setEnabled(False)
        
        #variabili utili
        self.controllo_mean=0
        self.controllo_clima=0
        self.controllo_aspect=0
        self.controllo_soil=0
        self.controllo_patch=0
        self.controllo_update=0
        self.controllo_modello=False
        self.controllo_dm=False
        
        #per lo stop del dm
        self.check_stop_dm=False
        
        #connessione segnali-slot
        
        self.dlg.actionPreferences.activated.connect(self.configuration_panel)
        self.dlg.actionAbout.activated.connect(self.about)
        self.dlg.actionHelp.activated.connect(self.help)
        #actionReclass
        self.dlg.actionReclass.activated.connect(self.reclass_panel)
        self.dlg.button_border.clicked.connect(self.process_border)
        self.dlg.buttonBox.accepted.connect(self.azione_udp)
        self.dlg.buttonBox.button(QtGui.QDialogButtonBox.Reset).clicked.connect(self.ricarica)
        
        self.dlg.buttonBox_output.button(QtGui.QDialogButtonBox.Reset).clicked.connect(self.reset_output)
        self.dlg.buttonBox_output.button(QtGui.QDialogButtonBox.Save).clicked.connect(self.esporta_output)
        
        
        self.dlg_conf.button_dbload.clicked.connect(lambda: self.scegli_file("sqlite"))
        
        self.dlg_conf.button_load_kmean.clicked.connect(lambda: self.scegli_file("csv"))
        
        self.dlg_reclass.button_in_reclass.clicked.connect(lambda: self.scegli_file("tutti"))
        self.dlg_reclass.button_out_reclass.clicked.connect(lambda: self.scegli_file("tif"))
        
        self.dlg.botton_save_m.clicked.connect(lambda: self.scegli_file("salvam"))
        self.dlg.button_save_f.clicked.connect(lambda: self.scegli_file("salvaf"))
        
        self.dlg_conf.check_climate.toggled.connect(self.scegli_clima)
        self.dlg_conf.check_aspect.toggled.connect(self.scegli_aspect)
        self.dlg_conf.check_patch.toggled.connect(self.scegli_patch)
        self.dlg_conf.check_soil.toggled.connect(self.scegli_soil)
        self.dlg_conf.check_mean.toggled.connect(self.scegli_kmean)

        self.dlg.button_graphall.setText("Plot M_tot")
        self.dlg.button_graphlu.setText("Plot M_belu")
        
        self.dlg.button_graphall.clicked.connect(self.plotallmodello)
        self.dlg.button_graphlu.clicked.connect(self.plotmodello)
        self.dlg.buttonBox_model.button(QtGui.QDialogButtonBox.Apply).clicked.connect(self.azionemodello)
        self.dlg.buttonBox_model.button(QtGui.QDialogButtonBox.Reset).clicked.connect(self.reset_output_model)
        self.dlg.buttonsave_csv.clicked.connect(lambda: self.scegli_file("salvacsv"))
        self.dlg.buttonsave_table.clicked.connect(lambda: self.scegli_file("salvacsv2"))
        self.dlg.buttonsave_dm.clicked.connect(lambda: self.scegli_file("salvacsvdm"))
        self.dlg.buttonsave_espath.clicked.connect(lambda: self.scegli_file("salvacsves"))
        self.dlg.buttonopen_csvpatch.clicked.connect(lambda: self.scegli_file("salvacsvpatch"))
        
        self.dlg.check_update.toggled.connect(self.schegli_update_shape)
        
        self.dlg.buttonBox_dm.button(QtGui.QDialogButtonBox.Apply).clicked.connect(self.azionedm)
        self.dlg.buttonBox_dm.button(QtGui.QDialogButtonBox.Reset).clicked.connect(self.reset_output_dm)
        #self.dlg.buttonBox_dm.button(QtGui.QDialogButtonBox.Abort).clicked.connect(self.stop_dm)
        
        self.dlg.spinModel.setRange(1, 999);  
        self.dlg.spinpatch_min.setRange(0, 999999);
        self.dlg.spinpatch_max.setRange(0, 999999);
        
        self.dlg.edit_costA.setText("1")
        self.dlg.edit_costB.setText("1")  
        
        self.dlg.lineEdit_3.setText("0")


        
        
        #da eliminare
        
        
        # self.dlg.lineEdit_csv.setText("/Users/francescogeri/Desktop/pandora_lavoro/lc.csv")     
        # self.dlg.lineEdit_table.setText("/Users/francescogeri/Desktop/pandora_lavoro/belu.csv")
        # self.dlg.lineEdit_dm.setText("/Users/francescogeri/Desktop/pandora_lavoro/dm.csv")   
        
        
        
        ##############da eliminare##################
        
        self.costanteA=1.5
        self.costanteB=1.5
        
        ############################################        
        
  

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/pandora/icon.png"),
            "Pandora", self.iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu("&Pandora", self.action)
        
        #self.dlg.buttonBox_dm.button(QtGui.QDialogButtonBox.Abort).clicked.connect(self.stop_dm)
      

    def stop_dm(self):
        self.check_stop_dm=True
            

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu("&Pandora", self.action)
        self.iface.removeToolBarIcon(self.action)

    #per lo stop del dm
    def sigint_handler(*args):
        """Handler for the SIGINT signal."""
        sys.stderr.write('\r')
        self.check_stop_dm=True

           
    
    def popolacombo(self):
        self.dlg.combovector.clear()
        self.dlg.comboudp.clear()
        self.dlg.comboclimate.clear()
        self.dlg.combosoil.clear()
        self.dlg.comboaspect.clear()
        self.allLayers = self.canvas.layers()
        self.listalayers=dict()
        self.elementovuoto="No required"
        self.dlg.comboclimate.addItem(self.elementovuoto)
        self.dlg.combosoil.addItem(self.elementovuoto)
        self.dlg.comboaspect.addItem(self.elementovuoto)
        for self.i in self.allLayers:
            self.listalayers[self.i.name()]=self.i
            if self.i.type() == QgsMapLayer.VectorLayer:            
                self.dlg.combovector.addItem(str(self.i.name()))
                self.dlg.comboudp.addItem(str(self.i.name())) 
            if self.i.type() == QgsMapLayer.RasterLayer:           
                self.dlg.comboclimate.addItem(str(self.i.name()))
                self.dlg.comboaspect.addItem(str(self.i.name()))   
                self.dlg.combosoil.addItem(str(self.i.name()))              
    
                     
    def acquisisci_layer(self):
        pass
        
    def ricarica(self):
        self.popolacombo()
    
    def checklayers(self):
        pass
    
    def configuration_panel(self):
        self.dlg_conf.show()
        self.result_conf=self.dlg_conf.exec_()
        if self.result_conf==1:
            self.db=self.dlg_conf.pathtodb.text()      
            
            
    def reclass_panel(self):
        self.dlg_reclass.show()           
            
    
    def scegli_clima(self):
        if self.controllo_clima==0:
            #print "clima attivo"
            self.controllo_clima=1
        else:
            #print "clima disattivato"
            self.controllo_clima=0
            
    def scegli_aspect(self):
        if self.controllo_aspect==0:
            #print "aspect attivo"
            self.controllo_aspect=1
        else:
            #print "aspect disattivato"
            self.controllo_aspect=0

    def scegli_soil(self):
        if self.controllo_soil==0:
            #print "aspect attivo"
            self.controllo_soil=1
        else:
            #print "aspect disattivato"
            self.controllo_soil=0
            
    def scegli_patch(self):
        if self.controllo_patch==0:
            #print "patch attivo"
            self.controllo_patch=1
        else:
            #print "patch disattivato"
            self.controllo_patch=0  
            
    def scegli_kmean(self):
        if self.controllo_mean==0:
            #print "patch attivo"
            self.controllo_mean=1
            self.dlg_conf.check_climate.setEnabled(False)
            self.dlg_conf.check_aspect.setEnabled(False)
            self.dlg_conf.check_patch.setEnabled(False)
            self.dlg_conf.path_to_kmean.setEnabled(True)
            self.dlg_conf.button_load_kmean.setEnabled(True)
        else:
            #print "patch disattivato"
            self.controllo_mean=0 
            self.dlg_conf.check_climate.setEnabled(True)  
            self.dlg_conf.check_aspect.setEnabled(True)
            self.dlg_conf.check_patch.setEnabled(True)              
            self.dlg_conf.path_to_kmean.setEnabled(False)   
            self.dlg_conf.button_load_kmean.setEnabled(False)

            
    def schegli_update_shape(self):
        if self.controllo_update==0:
            #print "update attivo"
            self.controllo_update=1
        else:
            #print "update disattivato"
            self.controllo_update=0         
        
            
    def about(self):         
        #self.credits = u"Università della Tuscia\n Viterbo - Italy\nRaffaele Pelorosso, Federica Gobattoni\nDeveloper: Francesco Geri"
        #QMessageBox.about(self.dlg,"Credits", self.credits ) 
        QMessageBox.about(self.dlg, "About Pandora","""<p>PANDORA release 1.0<br />8-5-2015<br />License: GPL v. 3<br /><a href='https://bitbucket.org/fragit/pandora'>Home page plugin</a></p><hr><p>Authors: Raffaele Pelorosso and Federica Gobattoni</p><p>Universita' della Tuscia, Viterbo - Dipartimento di scienze e tecnologie per l'Agricoltura, le Foreste, la Natura e l'Energia (DAFNE) <a href="http://www.dafne.unitus.it/">www.dafne.unitus.it</a> - <a href="https://www.facebook.com/groups/979138832174998/" target="blank">Facebook group</a></p><hr><p>Developer: Francesco Geri - Sistema Lab snc</p>""")          
    

    def help(self):         
        #self.credits = u"Università della Tuscia\n Viterbo - Italy\nRaffaele Pelorosso, Federica Gobattoni\nDeveloper: Francesco Geri"
        #QMessageBox.about(self.dlg,"Credits", self.credits ) 
        if platform.uname()[0]=="Windows":
            os.system("start "+os.path.dirname(__file__)+"/tutorial/tutorial_pandora3_1.pdf")
        if platform.uname()[0]=="Linux":
            os.system("xdg-open "+os.path.dirname(__file__)+"/tutorial/tutorial_pandora3_1.pdf")
        else:
            os.system("open "+os.path.dirname(__file__)+"/tutorial/tutorial_pandora3_1.pdf")



    def scegli_file(self,tipofile):
        self.tipofile=tipofile
        if self.tipofile=="sqlite":
            self.fname = QFileDialog.getOpenFileName(None, 'Open file', '/home','sqlite3 files (*.sqlite);;all files (*.*)')
            self.dlg_conf.pathtodb.setText(self.fname) 
        if self.tipofile=="csv":
            self.fname = QFileDialog.getOpenFileName(None, 'Open file', '/home','csv files (*.csv);;all files (*.*)')
            self.dlg_conf.path_to_kmean.setText(self.fname)            
        if self.tipofile=="tif":
            self.fname = QFileDialog.getOpenFileName(None, 'Open file', '/home','GeoTiff files (*.tif);;all files (*.*)')
            self.dlg_reclass.output_raster_class.setText(self.fname)            
        if self.tipofile=="tutti":
            self.fname = QFileDialog.getOpenFileName(None, 'Open file', '/home','all files (*.*)')
            self.dlg_reclass.input_reclass.setText(self.fname)    
        if self.tipofile=="salvam":
            self.fname = QFileDialog.getSaveFileName(None, 'Save file', '/home','all files (*.*)')
            self.dlg.lineEdit.setText(self.fname)      
        if self.tipofile=="salvaf":
            self.fname = QFileDialog.getSaveFileName(None, 'Save file', '/home','all files (*.*)')
            self.dlg.lineEdit_2.setText(self.fname)
        if self.tipofile=="salvacsv":
            self.fname = QFileDialog.getSaveFileName(None, 'Save file', '/home','csv files (*.csv);;all files (*.*)')
            self.dlg.lineEdit_csv.setText(self.fname)
        if self.tipofile=="salvacsv2":
            self.fname = QFileDialog.getSaveFileName(None, 'Save file', '/home','csv files (*.csv);;all files (*.*)')
            self.dlg.lineEdit_table.setText(self.fname)    
        if self.tipofile=="salvacsvdm":
            self.fname = QFileDialog.getSaveFileName(None, 'Save file', '/home','csv files (*.csv);;all files (*.*)')
            self.dlg.lineEdit_dm.setText(self.fname) 
        if self.tipofile=="salvacsves":
            self.fname = QFileDialog.getSaveFileName(None, 'Save file', '/home','csv files (*.csv);;all files (*.*)')
            self.dlg.lineEdit_es.setText(self.fname) 
        if self.tipofile=="salvacsvpatch":
            self.fname = QFileDialog.getOpenFileName(None, 'Open file', '/home','csv files (*.csv);;all files (*.*)')
            self.dlg.lineEdit_patchcsv.setText(self.fname)                                                     
            
    def process_border(self):
        
        self.testovector = str(self.dlg.combovector.currentText())
        self.testoudp = str(self.dlg.comboudp.currentText())
        
        if self.testovector=="" or self.testoudp=="":
            QMessageBox.warning(self.dlg,"Warning", "Check layers\nLC/LU map and BELU map are required" )
            self.controllolayers=1
            return      
        
        self.calcola_bordo()  
        self.dlg_table=tableDialog(self.diz_confronto)
        self.dlg_table.show()

    
    def calcola_bordo(self):      
        self.testoudp = str(self.dlg.comboudp.currentText())        
        if self.testoudp=="":
            QMessageBox.warning(self.dlg,"Warning", "Check layers\nLC/LU map and BELU map are required" )
            return

        self.udp=self.listalayers[self.testoudp]       
        self.iter2=self.feat2=self.feat1=self.geom2=None
        self.j=0
        self.k=0
        self.diz_confronto=dict()
        self.iter1=self.udp.getFeatures()
        
        self.feat1 = QgsFeature()
        
        self.idx_udp = self.udp.fieldNameIndex('belu')


        if os.path.isfile('/Users/francescogeri/Desktop/prova_bordi.shp'):
            os.remove('/Users/francescogeri/Desktop/prova_bordi.shp')

        vl = QgsVectorLayer("MultiLineString", "commonborder", "memory")
        pr = vl.dataProvider()

        pr.addAttributes([QgsField("id",  QVariant.String),QgsField("length",  QVariant.Double)])

        # driver_border = ogr.GetDriverByName('ESRI Shapefile')
        # datasource_border = driver_border.CreateDataSource('/Users/francescogeri/Desktop/prova_bordi.shp')
        # layer_border = datasource_border.CreateLayer('commonborder',geom_type=ogr.wkbMultiLineString)
        #layer_border.addAttributes([QgsField("name", QVariant.String)])
        
        for self.feat1 in self.iter1:
            self.iter2=self.udp.getFeatures()
            self.feat2 = QgsFeature()
            self.geom1 = self.feat1.geometry()
            self.attrs1 = self.feat1.attributes()
            self.attributo1=self.attrs1[self.idx_udp]
            for self.feat2 in self.iter2:
                
                self.attrs2 = self.feat2.attributes()
                self.attributo2=self.attrs2[self.idx_udp]
                
                if self.attributo1!=self.attributo2:
                    self.confronto1=str(self.attributo1)+"-"+str(self.attributo2)
                    self.confronto2=str(self.attributo2)+"-"+str(self.attributo1)
                    if self.diz_confronto.has_key(self.confronto1)==True or self.diz_confronto.has_key(self.confronto2)==True:
                        pass
                    else:
                        self.geom2 = self.feat2.geometry()
                        #intersects()                        
                        self.commonborder=self.geom1.intersection(self.geom2)
                        if self.commonborder.length()>0.0:
                            self.lunghezza=float(self.commonborder.length())
                            self.diz_confronto[str(self.confronto1)]=self.lunghezza
                            #self.aggiorna_output(str(self.commonborder.asMultiPolyline()))
                            outfeat = QgsFeature()
                            #outfeat.setAttributes(0,"hello")
                            outfeat.setGeometry(self.commonborder)
                            outfeat.setAttributes([self.confronto1,self.lunghezza])
                            pr.addFeatures([outfeat])
                            #layer_border.dataProvider().addFeatures([outfeat])
                            #(res, outFeats) = layer_border.dataProvider().addFeatures([outfeat])
            
            vl.commitChanges()
            vl.updateExtents()
            vl.updateFields()
            QgsMapLayerRegistry.instance().addMapLayer(vl)
            self.iter2=self.feat2=self.geom2=None
         
        
        #self.vettoriale e self.udp        
        
    def leggicsvkmean(self,diz_udp_xcontrollo):
        fnamecsv = self.pathtocsvkmean
        try:
            self.filekappacsv=open(fnamecsv,"r")
        except:
            return        
            
        lettore=csv.reader(self.filekappacsv, delimiter='|')
        contatorefile=0
        numudp=len(diz_udp_xcontrollo)
        
        csv.register_dialect('csv_pandora', delimiter='|', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        
        for riga in lettore:
            contatorefile+=1
        if contatorefile!=numudp:
            #QtGui.QMessageBox.critical(self,"Error", "Error number of rows in csv kmean file")
            QMessageBox.critical(self.dlg,"Error", "Error number of rows in csv kmean file" ) 
            return 0
        self.filekappacsv.close()
        lettore=riga=contatorefile=None
        self.filekappacsv=open(fnamecsv,"r")        
        lettore=csv.reader(self.filekappacsv,dialect="csv_pandora")
        result_kjmedio="\nAVERAGE KAPPA ANALYSIS\n\n"

        for riga in lettore:
            conto_var=0
            value_medio=0            
            nome_udp=int(riga[0])
            try:
                value1=riga[1]
                conto_var+=1
                #print "valore 1 "+str(value1)
                #print conto_var
            except:
                value1=0
            try:
                value2=riga[2]
                conto_var+=1
                #print "valore 2 "+str(value2)
                #print conto_var                
            except:
                value2=0
            try:
                value3=riga[3]
                conto_var+=1
                #print "valore 3 "+str(value3)
                #print conto_var                
            except:
                value3=0
            try:
                value4=riga[4]
                conto_var+=1
                #print "valore 3 "+str(value4)
                #print conto_var                
            except:
                value4=0    
            
            if conto_var==0:
                QMessageBox.critical(self.dlg,"Error", "csv kmean file not correct" )
                return 0 
                 
            value_medio=(float(value1)+float(value2)+float(value3)+float(value4))/conto_var 
            #print "valore medio "+str(value_medio)                                          
            #value=riga[1]
            if self.diz_kappaj.has_key(nome_udp):
                self.diz_kappaj[nome_udp]=float(value_medio) 
                result_kjmedio+="BELU code %s " %str(nome_udp)
                result_kjmedio+="average kappa = %s" %str(value_medio)
                result_kjmedio+="\n"                    
            else:
                #QtGui.QMessageBox.critical(self,"Error", "csv kmean file not correct") 
                QMessageBox.critical(self.dlg,"Error", "csv kmean file not correct" )   
                return 0  
        
        result_kjmedio+="---------------------------\n"
        return result_kjmedio      

    def azione_udp(self):       
        self.messaggiopre=""
        self.risultato=""
        self.controllo=0
        
        #controllo db
        self.pathtodb=self.dlg_conf.pathtodb.text()
        self.pathtocsvkmean=self.dlg_conf.path_to_kmean.text()
        
        if self.pathtodb=="":
            QMessageBox.warning(self.dlg,"Warning", "Check configuration panel\nDatabase required" )
            return        
        
        
        self.controllo_costanti=self.controllo_aspect+self.controllo_clima+self.controllo_soil+self.controllo_patch+self.controllo_mean
        
        if (self.controllo_mean==1):
            if self.pathtocsvkmean=='':
                QMessageBox.warning(self.dlg,"Warning", "Check configuration panel\nKmean csv file is checked" )
                return     
            if os.path.exists(self.pathtocsvkmean):
                pass
            else:
                QMessageBox.warning(self.dlg,"Warning", "Check configuration panel\nKmean csv file doesn't exist" )
                return                
                
        
        #controllo checkbox
        
        if self.controllo_costanti==0:
            QMessageBox.warning(self.dlg,"Warning", "Check configuration panel\nAt least one constant required" )
            return
        
        #controllo layers
                
        self.controllo=self.controllo_layers()
        if self.controllo==1:
            return                
        
        try:
            self.diz_permeability=self.dlg_table.richiesta_valori()
        except:
            QMessageBox.warning(self.dlg,"Warning", "Need to complete process border analisys" )
            return
        
        #self.dlg.sb.showMessage(self.dlg.tr("Performing analysis"))  
        self.msg = self.msgBar.createMessage( u'Performing analysis' )
        self.msgBar.pushWidget( self.msg, QgsMessageBar.INFO, 10 )
                      
        self.messaggiopre+="START ANALYSIS\n"
        self.messaggiopre+="\n---------------------------\n"
        
        start_time=time.time()
        
        self.aggiorna_output(self.messaggiopre)
        
        self.controllo,self.risultato=self.controllo_db()
        if self.controllo==1:
            self.aggiorna_output(self.risultato)
            return
        
        self.aggiorna_output(self.risultato)
        
        pandora1=UdP(self.controllo_clima,self.controllo_aspect,self.controllo_soil,self.controllo_patch,self.pathtodb,self.vettoriale,self.udp,self.clima,self.aspect,self.path_m,self.path_f)
        
        self.dizionario2,self.dizionario3,self.dizionario4=pandora1.estrai_clc()
        
        #print self.dizionario2,self.dizionario3,self.dizionario4
        
        self.risultato,self.diz_udp,self.diz_udp_btc,self.vettoriale_path,self.diz_U,self.diz_perimetritot0,self.diz_perimetritot,self.areatot,self.numeroudp=pandora1.analisiudp(self.dizionario2,self.dizionario3,self.dizionario4)
        
        self.aggiorna_output(self.risultato) 
        
        if self.diz_udp==0:
            QMessageBox.warning(self.dlg,"Warning", "Shapefiles not correct" )
            return
                 
        
        self.udp_path=string.split(self.udp.dataProvider().dataSourceUri(), "|")  

        self.diz_kappaesp=dict()
        self.diz_kappaclima=dict()
        self.diz_kappapatch=dict()
        self.diz_kappasoil=dict()
        
        #*****************************************
        #da eliminare
        #temporanea
        self.diz_udp=pandora1.calcolareaudp()        
        #*****************************************
        
        
        #print "dizionario perimetro"
        self.calcolaperimetro()        
        
        self.diz_kappaesp=pandora1.inizializzadiz(self.diz_kappaesp)
        self.diz_kappaclima=pandora1.inizializzadiz(self.diz_kappaclima)
        self.diz_kappapatch=pandora1.inizializzadiz(self.diz_kappapatch)
        self.diz_kappasoil=pandora1.inizializzadiz(self.diz_kappasoil)
        
        if self.controllo_mean==0:       
            if self.controllo_clima==1:
                self.raster_path=self.clima.dataProvider().dataSourceUri()
                self.analisi_clima=raster_udp(str(self.udp_path[0]),self.raster_path) 
                self.result_analizzaclima,self.diz_kappaclima=self.analisi_clima.analizza_raster("clima",self.areatot)
                self.aggiorna_output(self.result_analizzaclima)   
         
            if self.controllo_aspect==1:
                self.raster_path=self.aspect.dataProvider().dataSourceUri()
                self.analisi_clima=raster_udp(self.udp_path[0],self.raster_path) 
                self.result_analizzaesp,self.diz_kappaesp=self.analisi_clima.analizza_raster("aspect",self.areatot)
                self.aggiorna_output(self.result_analizzaesp) 

            

            if self.controllo_soil==1:
                self.raster_path=self.soil.dataProvider().dataSourceUri()
                self.analisi_clima=raster_udp(self.udp_path[0],self.raster_path) 
                self.result_analizzasoil,self.diz_kappasoil=self.analisi_clima.analizza_raster("soil",self.areatot)
                self.aggiorna_output(self.result_analizzasoil) 
                
            

            if self.controllo_patch==1:
                self.result_analizzapatch,self.diz_kappapatch=pandora1.calcolaKpatch(self.diz_kappapatch,self.diz_perimetritot,self.diz_perimetri)
                self.aggiorna_output(self.result_analizzapatch)
            #print "dizionario permeability"
            #print self.diz_permeability
            
            #print "dizionario kappaesp"
            #print self.diz_kappaesp
            
            #print "dizionario kappaclima"
            #print self.diz_kappaclima               
                    
            self.diz_kappaj,resultkmedio=pandora1.analisiKJ(self.controllo_costanti,self.diz_kappaclima,self.diz_kappaesp,self.diz_kappapatch,self.diz_kappasoil)
                
        else:
            self.diz_kappaj=dict()
            self.diz_kappaj=pandora1.inizializzadiz(self.diz_kappaj)
            resultkmedio=self.leggicsvkmean(self.diz_udp)
            if resultkmedio==0:
                return
        
        self.aggiorna_output(resultkmedio)
        #print "dizionario kappaj"
        #print self.diz_kappaj        

        ##### test analisi raster on vettoriale uso suolo
        ##### tempo impiegato: 653.653408051 seconds
        """
        print "\n------inizio analisi raster vettoriale-------\n"
        self.pathvector_prova="/home/francesco/Scrivania/lu1.shp"
        start_time = time.time()
        self.raster_path=self.clima.dataProvider().dataSourceUri()
        self.analisi_plus=raster_udp(self.pathvector_prova,self.raster_path)
        self.result_analizzaprova,self.diz_kappaprova=self.analisi_plus.analizza_raster("clima")
        print time.time() - start_time, "seconds"
        """
        
            
        self.diz_udp_M,self.resultM,self.diz_udp_Mmax=pandora1.risultatoM(self.diz_kappaj,self.diz_udp_btc)        
        
        
        #print "dizionario M"
        #print self.diz_udp_M
        
        #print "dizionario Mmax"
        #print self.diz_udp_Mmax        
        
        self.aggiorna_output(self.resultM)

        
        self.diz_h=dict()
        self.diz_h=pandora1.inizializzadiz(self.diz_h)
        
        self.diz_h=pandora1.calcolah(self.diz_h,self.diz_perimetri,self.diz_perimetritot0)
        
        """
        print "perimetri con btc=0"
        print self.diz_perimetritot0
        
        print "dizionario h"
        print self.diz_h
        
        print "dizionario U"
        print self.diz_U         
        
        print "dizionario bordo"
        print self.diz_confronto       
        """
        
        self.diz_flussi,self.resultF,self.diz_flussi_max=pandora1.calcola_flusso(self.diz_udp_M,self.diz_confronto,self.diz_permeability,self.diz_perimetri,self.diz_udp_Mmax)
        
        """
        print "dizionario flussi"
        print self.diz_flussi
        
        print "dizionario flussi max"
        print self.diz_flussi_max  
        
        """
        
        self.diz_sommaF=dict() 
        self.diz_sommaFmax=dict()  
        self.diz_sommaF=pandora1.inizializzadiz(self.diz_sommaF)
        self.diz_sommaFmax=pandora1.inizializzadiz(self.diz_sommaFmax)   
        
        self.diz_sommaF,self.diz_sommaFmax=pandora1.sommaF(self.diz_sommaF,self.diz_sommaFmax,self.diz_flussi,self.diz_flussi_max)
        
        """
        print "dizionario sommaF"
        print self.diz_sommaF
        print "dizionario sommaFmax"          
        print self.diz_sommaFmax  
        """
        
        self.diz_C=pandora1.costanteC(self.diz_sommaF,self.diz_sommaFmax) 
        
        #print "costante C"
        #print self.diz_C         
        
        self.aggiorna_output(self.resultF)
        
        self.result_buildoutM,self.diz_centroidi,self.vector_M=pandora1.outputM(self.path_m)
        
        QgsMapLayerRegistry.instance().addMapLayer(self.vector_M)      
        
        #print "dizionario centroidi"
        #print self.diz_centroidi  
        
        self.vector_F=pandora1.outputF(self.diz_flussi,self.diz_centroidi,self.path_f)
        
        QgsMapLayerRegistry.instance().addMapLayer(self.vector_F)
        
        self.aggiorna_output(self.result_buildoutM)
        
        
        self.msg = self.msgBar.createMessage( u'Graph correctly executed' )
        
        #print "tempo inizializzazione" 
        #print time.time() - start_time        
        
        self.controllo_modello=True
        
        self.pandora_mod=pandora1
        
        
        

        

    def controllo_layers(self):
        self.controllolayers=0
        self.testovector = str(self.dlg.combovector.currentText())
        self.testoudp = str(self.dlg.comboudp.currentText())
        self.testoclimate = str(self.dlg.comboclimate.currentText())
        self.testoaspect = str(self.dlg.comboaspect.currentText())
        self.testosoil = str(self.dlg.combosoil.currentText())
        
        if self.testovector=="" or self.testoudp=="":
            QMessageBox.warning(self.dlg,"Warning", "Check layers\nLC/LU map and BELU map are required" )
            self.controllolayers=1
            return self.controllolayers
        
        if self.testoclimate=="No required" and self.controllo_clima==1:
            QMessageBox.warning(self.dlg,"Warning", "Climate constant checked\nClimate map is required" )
            self.controllolayers=1
            return self.controllolayers
        
        if self.testoaspect=="No required" and self.controllo_aspect==1:
            QMessageBox.warning(self.dlg,"Warning", "Aspect constant checked\nAspect map is required" )
            self.controllolayers=1
            return self.controllolayers 

        if self.testosoil=="No required" and self.controllo_soil==1:
            QMessageBox.warning(self.dlg,"Warning", "Soil constant checked\Soil map is required" )
            self.controllolayers=1
            return self.controllolayers     
        
        self.udp=self.listalayers[self.testoudp]
        self.vettoriale=self.listalayers[self.testovector]
        
        if self.testoclimate!="No required":
            self.clima=self.listalayers[self.testoclimate]
        else:
            self.clima=0
        if self.testoaspect!="No required":        
            self.aspect=self.listalayers[self.testoaspect]
        else:
            self.aspect=0

        if self.testosoil!="No required":        
            self.soil=self.listalayers[self.testosoil]
        else:
            self.soil=0

            
        #self.dlg.lineEdit.setText(self.fname)
        #self.db=self.dlg_conf.pathtodb.text()
        self.path_m=self.dlg.lineEdit.text()
        self.path_f=self.dlg.lineEdit_2.text()
        if self.path_m=="":
            self.path_m=os.path.dirname(__file__)+"/M.shp"
        if self.path_f=="":
            self.path_f=os.path.dirname(__file__)+"/F.shp"       
        
        
        #inserire controllo per verificare se i vettoriali sono corretti   
        
                    
    def controllo_db(self): 
        self.controllodb=0  
        self.risultatodb=""  
        self.statodb=str(isfile(self.pathtodb))
        if self.statodb=="False":
            self.risultatodb+="\nDatabase not found\n\n"
            self.risultatodb+="DATABASE ANALYSIS NOT PERFORMED\n"  
            self.risultatodb+="---------------------------\n"           
            self.controllodb=1
            return self.controllodb,self.risultatodb
        else:
            self.risultato+="\nDatabase found\n\n"
        self.fd = open(self.pathtodb, 'rb')
        self.header = self.fd.read(100)
        if self.header[0:16]=="SQLite format 3":
            self.risultatodb+="\nPROBLEMS WITH READING DATABASE\n"  
            self.risultatodb+="---------------------------\n"          
            self.controllodb=1
            return self.controllodb,self.risultatodb
        else:
            self.risultatodb+="Database rightly configured"               
        self.conn = sqlite3.connect(self.pathtodb)  
        return self.controllodb,self.risultatodb             
    
            
    def aggiorna_output(self,appendi):    
        self.appendi=appendi
        self.dlg.textoutput.appendPlainText(self.appendi)  
        
    def aggiorna_output_model(self,appendi):    
        self.dlg.textmodel.appendPlainText(appendi)      
        
    def aggiorna_output_dm(self,appendi):    
        self.dlg.textdm.appendPlainText(appendi)            
        
    def reset_output(self):    
        self.dlg.textoutput.clear()
        
    def reset_output_model(self):    
        self.dlg.textmodel.clear() 
        self.dlg.lineEdit_csv.clear() 
        
    def reset_output_dm(self):    
        self.dlg.textdm.clear() 
        self.dlg.lineEdit_dm.clear()     
        self.dlg.lineEdit_es.clear()          
        
        
    def esporta_output(self):
        # try:
        self.resultudp=self.dlg.textoutput.toPlainText()
        self.dlg.textoutput.selectAll()
        self.dlg.textoutput.copy()
        #QMessageBox.information(self.dlg,"Info", "Ctrl+c to copy the selected text" )
        # self.f_res_export=open("resultpandora.txt","w")
        # self.f_res_export.write(self.resultudp)
        # self.f_res_export.close()
        # QMessageBox.information(self.dlg,"Info", "File resultpandora.txt exported in the working folder" )            
        # except:
        #     QMessageBox.information(self.dlg,"Info", "Nothing to export" )   
            
    def calcolaperimetro(self):
        self.diz_perimetri=dict()
        self.iter=self.feat=self.geom=self.attrs=self.nomeudp=None
        self.iter=self.udp.getFeatures()
        self.idx_udp = self.udp.fieldNameIndex('belu')
        self.feat = QgsFeature()
        for self.feat in self.iter:
            self.attrs = self.feat.attributes()
            self.nomeudp=self.attrs[self.idx_udp]
            self.diz_perimetri[int(self.nomeudp)]=self.feat.geometry().length()
            
        #print self.diz_perimetri
    
            
    #da eliminare    
    def plot(self):
        ''' plot some random stuff '''
        # random data
        data = [random.random() for i in range(10)]

        # create an axis
        ax = self.dlg.figure.add_subplot(111)

        # discards the old graph
        ax.hold(False)

        # plot data
        ax.plot(data, '*-')

        # refresh canvas
        self.dlg.canvas.draw()   
        
        
    
    def azionemodello(self):
        if self.controllo_modello==False:
            QMessageBox.warning(self.dlg,"Warning", "Complete Base Parameters module" )
            return        
        self.part_to_csv=self.dlg.lineEdit_csv.text()
        self.part_to_table=self.dlg.lineEdit_table.text()
        
        self.Acostante=self.dlg.edit_costA.text()
        self.Bcostante=self.dlg.edit_costB.text()
        try:
            self.costanteA=float(self.Acostante)
        except:
            QMessageBox.warning(self.dlg,"Warning", "Check costant values" )
            return            
            
        try:
            self.costanteB=float(self.Bcostante)
        except:
            QMessageBox.warning(self.dlg,"Warning", "Check costant values" )
            return
                    
        self.cicli=self.dlg.spinModel.text()
        if self.part_to_csv=="" or self.part_to_table=="":
            QMessageBox.warning(self.dlg,"Warning", "csv output file is required" )
            return
        if int(self.cicli)<1:
            QMessageBox.warning(self.dlg,"Warning", "at least 1 cycle is required" )
            return  

        self.aggiorna_output_model("----------------")
        self.aggiorna_output_model("Start modelling\n")
        self.aggiorna_output_model("Number of cycles: "+str(self.cicli))
        
        start_time = time.time()
        
        #self.diz_udp_M,self.resultM,self.diz_udp_Mmax=pandora1.risultatoM(self.controllo_costanti,self.diz_udp_btc,self.diz_kappaclima,self.diz_kappaesp,self.diz_kappapatch)
        #return self.diz_patch_udp,self.diz_patch_area,self.diz_patch_B,self.diz_patch_K,self.diz_patch_M,self.diz_patch_Mmax,self.diz_udp_M2,self.diz_udp_M2max
        self.diz_patch_udp,self.diz_patch_area,self.diz_patch_B,self.diz_patch_K,self.diz_patch_M,self.diz_patch_Mmax,self.diz_udp_M2,self.diz_udp_M2max,self.diz_udp_C,self.diz_patch_clc=self.pandora_mod.inizioModello(self.diz_h,self.diz_U,self.diz_C,self.diz_kappaj,self.diz_confronto,self.diz_permeability,self.diz_perimetri,self.costanteA,self.costanteB)

        self.diz_patch_Bdm=deepcopy(self.diz_patch_B) #permette di copiare 2 dict senza mantenere il link
        
        
               

        self.cicli=int(self.cicli)
        
        self.Mas,self.diz_udpMfinal,self.diz_udpCfinal,self.diz_patch_B=self.pandora_mod.cicloModello(self.diz_kappaj,self.diz_patch_udp,self.diz_patch_area,self.diz_patch_B,self.diz_patch_K,self.diz_patch_M,self.diz_patch_Mmax,self.diz_udp_M2,self.diz_udp_M2max,self.diz_udp_C,self.diz_U,self.diz_h,self.cicli,self.dlg,"mod",self.costanteA,self.costanteB)
        
        
        self.pandora_mod.salvamodello(self.cicli,self.diz_udpMfinal,self.diz_udp_M2max,self.part_to_csv)
        self.pandora_mod.salvapatchmodello(self.cicli,self.diz_patch_udp,self.diz_patch_B,self.diz_patch_area,self.part_to_table)
        
        #print "tempo modellizzazione"
        self.tempo=time.time() - start_time
        #print self.tempo
        
        self.aggiorna_output_model("Modelling time: "+str(self.tempo))
        self.aggiorna_output_model("\nFinal csv table saved in: ")
        self.aggiorna_output_model(self.part_to_csv)
        self.aggiorna_output_model("\nFinal csv patch table saved in: ")
        self.aggiorna_output_model(self.part_to_table)        
        self.aggiorna_output_model("\nNow is possible to plot the graph model")
        self.aggiorna_output_model("---------------")
        self.plotallmodello()

        
        self.controllo_dm=True
        self.dlg.spindm.setRange(1, self.cicli);
        self.dlg.spindm.setValue(self.cicli);
        
        self.lunghezza_patch=len(self.diz_patch_B)
        self.dlg.spinpatch_max.setValue(self.lunghezza_patch)
        
        
        self.pandora_mod.modifyM(self.vector_M,self.diz_udpMfinal,self.cicli,"belu","Mfinal")
        
        if self.controllo_update==1:                
            self.pandora_mod.aggiornapatch(self.cicli,self.diz_patch_B,self.diz_patch_area)
        
        
        self.diz_grafico=self.diz_udpMfinal

        
        for key,value in self.diz_udpMfinal.iteritems():
            lunghezza=len(value)
            for i in range(0,lunghezza):
                self.diz_grafico[key][i]=self.diz_udpMfinal[key][i]/self.diz_udp_M2max[key]          
        
        
        #---------------------------------------------
        #codice per il salvataggio in csv per i test
        """
        
        self.file1=open("/home/francesco/1.csv", 'wb')
        self.file2=open("/home/francesco/2.csv", 'wb')
        self.file3=open("/home/francesco/3.csv", 'wb')
        self.file4=open("/home/francesco/4.csv", 'wb')
        self.file5=open("/home/francesco/5.csv", 'wb')
        self.file6=open("/home/francesco/6.csv", 'wb')
        self.file7=open("/home/francesco/7.csv", 'wb')
        self.writer=csv.writer(self.file1, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in self.diz_udpCfinal[1]:  
            self.writer.writerow([self.item])
            
        self.writer=csv.writer(self.file2, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in self.diz_udpCfinal[2]:  
            self.writer.writerow([self.item])
            
        self.writer=csv.writer(self.file3, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in self.diz_udpCfinal[3]:  
            self.writer.writerow([self.item])
            
        self.writer=csv.writer(self.file4, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in self.diz_udpCfinal[4]:  
            self.writer.writerow([self.item])
            
        self.writer=csv.writer(self.file5, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        for self.item in self.diz_udpCfinal[5]:  
            self.writer.writerow([self.item])
            
        self.writer=csv.writer(self.file6, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        for self.item in self.diz_udpCfinal[6]:  
            self.writer.writerow([self.item])
            
        self.writer=csv.writer(self.file7, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        for self.item in self.diz_udpCfinal[7]:  
            self.writer.writerow([self.item])                                                           

        self.file1.close()        
        self.file2.close()
        self.file3.close()
        self.file4.close()
        self.file5.close()
        self.file6.close()
        self.file7.close()
        
        
        
        self.file1m=open("/home/francesco/1m.csv", 'wb')
        self.file2m=open("/home/francesco/2m.csv", 'wb')
        self.file3m=open("/home/francesco/3m.csv", 'wb')
        self.file4m=open("/home/francesco/4m.csv", 'wb')
        self.file5m=open("/home/francesco/5m.csv", 'wb')
        self.file6m=open("/home/francesco/6m.csv", 'wb')
        self.file7m=open("/home/francesco/7m.csv", 'wb')
        self.writer=csv.writer(self.file1m, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in self.diz_udpMfinal[1]:  
            self.writer.writerow([self.item])
        self.writer=csv.writer(self.file2m, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in self.diz_udpMfinal[2]:  
            self.writer.writerow([self.item])
        self.writer=csv.writer(self.file3m, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in self.diz_udpMfinal[3]:  
            self.writer.writerow([self.item])
        self.writer=csv.writer(self.file4m, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in self.diz_udpMfinal[4]:  
            self.writer.writerow([self.item]) 
            
        self.writer=csv.writer(self.file5m, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in self.diz_udpMfinal[5]:  
            self.writer.writerow([self.item])
            
        self.writer=csv.writer(self.file6m, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in self.diz_udpMfinal[6]:  
            self.writer.writerow([self.item])                        
            
        self.writer=csv.writer(self.file7m, delimiter='|',quoting=csv.QUOTE_ALL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in self.diz_udpMfinal[7]:  
            self.writer.writerow([self.item])                                                

        self.file1m.close()        
        self.file2m.close()
        self.file3m.close()
        self.file4m.close()  
        self.file5m.close()
        self.file6m.close()
        self.file7m.close() 
        
        

        
        #fine codice salvataggio in csv per i test
        #--------------------------------------------------        
        
        """
        
        
        
    def plotallmodello(self):
        try:
            self.diz_udpMfinal
        except:            
            QMessageBox.warning(self.dlg,"Warning", "No model to plot" )
            return  
        
        
        self.dlg.figure.clear()
        
        
        self.ax = self.dlg.figure.add_subplot(111)
        

        self.ax.hold(True)   
        
             
        self.key=self.value=None    
        
        self.key=self.value=i=None
        
        self.listadati=[]
        
        for i in range(1,self.cicli):
            valore=0
            for self.key in self.diz_udpMfinal.keys():
                valore+=self.diz_udpMfinal[self.key][i]
            self.listadati.append(valore)

        maxvalue=max(self.listadati)

        listadatistd=[]

        for x in self.listadati:
            value=x/maxvalue
            listadatistd.append(value)

        ####### check lista dati sul primo qtplaintextedit
        # str1 = ''.join(str(e) for e in self.listadati)
        # self.aggiorna_output(str1)
        #######

        
        self.ax.plot(listadatistd,'*-',label="model")
        legend(prop={'size':8})
        self.dlg.canvas.draw() 
            
                
        
    
        
    def plotmodello(self):
        try:
            self.diz_grafico
        except:            
            QMessageBox.warning(self.dlg,"Warning", "No model to plot" )
            return   


        self.dlg.figure.clear()
        
        
        self.ax = self.dlg.figure.add_subplot(111)

        

        self.ax.hold(True)   
        
             
        key=value=None
     
        
        for key,value in self.diz_grafico.iteritems():
            data=value
            self.ax.plot(data, '-',label=key)
        

        # refresh canvas
        legend(prop={'size':8})
        self.dlg.canvas.draw()          

    
    
    def caricapatch_csv(self,filepatchcsv):
        
        try:
            filecsv=open(filepatchcsv,"r")
        except:
            QMessageBox.warning(self.dlg,"Warning", "Patch csv file don't found or not correct" )
            return
        filecsv=open(filepatchcsv,"r")        
        lettore=csv.reader(filecsv,delimiter=',')
        listapatchcsv=[]
        for riga in lettore:                
            item_p = riga[0]
            listapatchcsv.append(int(item_p))
            #self.tableWidget.setItem(self.contatorefile, 3, item_p)   
            #self.contatorefile+=1   
            
        return listapatchcsv     
    
    
    
    def azionedm(self):
        
        self.controllopathcsv=False
        self.filepatchcsv=self.dlg.lineEdit_patchcsv.text()
        
        if self.filepatchcsv!="":
            listapatchcsv=self.caricapatch_csv(self.filepatchcsv)
            self.controllopathcsv=True

        
        self.diz_patch_dm=deepcopy(self.diz_patch_Bdm)


        self.stpedm=int(self.dlg.spindm.text())
        
        start_step=int(self.dlg.spinpatch_min.text())
        stop_step=int(self.dlg.spinpatch_max.text())
        
        dm0default=self.dlg.lineEdit_3.text()
        
        try:
            defaultdm0=float(dm0default)
        except:
            QMessageBox.warning(self.dlg,"Warning", "check the parameters of analyze" )
            return
        
        steptemporali=len(self.diz_patch_Bdm)
        
        
        if stop_step<=start_step:
            if stop_step==start_step:
                stop_step=int(steptemporali-1)
            else:
                QMessageBox.warning(self.dlg,"Warning", "check the parameters of analyze" )
                return                 
                
        
        if (stop_step==0 and start_stop>0):
            QMessageBox.warning(self.dlg,"Warning", "check the parameters of analyze" )
            return               
            
            
        
        self.path_to_dm=self.dlg.lineEdit_dm.text()
        if self.path_to_dm=="":
            QMessageBox.warning(self.dlg,"Warning", "csv output file is required" )
            return     

            
        
        self.dlg.progressBar_dm2.setValue(0)
        
        self.dlg.progressBar_dm2.setMaximum(stop_step-start_step)                      

        
        self.aggiorna_output_dm("----------------\n\n")
        self.aggiorna_output_dm("Start analysis dM\n")
        #self.aggiorna_output_dm(str(defaultdm0))
        contemp=0
        for ch,val in self.diz_patch_Bdm.iteritems():
            if ch>=start_step and ch<=stop_step:
                
                if self.controllopathcsv==True:
                    #print "controllo ok "+str(ch)
                    #print "index numero "+str(listapatchcsv.index(ch))
                    
                    try:
                        listapatchcsv.index(ch)
                
                        listadm=self.diz_patch_dm[ch]
                        contemp+=1
                        self.dlg.progressBar_dm2.setValue(contemp)
                        
                        #per lo stop del dm
                        
                        if self.check_stop_dm==True:
                            return
                        #fine per lo stop del dm
                        
                        if contemp==1:
                            start_time = time.time()                
                        #listadm.append(self.Mas)    
                        if val[0]==0.0:
                            listadm.append(self.Mas)
                            listadm.append(0)
                            self.diz_patch_dm[ch]=listadm
                        else:
            
                            #raw_input("attesa")
                            #import pdb; pdb.set_trace()
                            self.diz_patch_udp,self.diz_patch_area,diz_patch_Bdm,self.diz_patch_K,self.diz_patch_M,self.diz_patch_Mmax,self.diz_udp_M2,self.diz_udp_M2max,self.diz_udp_C=self.pandora_mod.inizioModello(self.diz_h,self.diz_U,self.diz_C,self.diz_kappaj,self.diz_confronto,self.diz_permeability,self.diz_perimetri,self.costanteA,self.costanteB)
                            diz_lavoro_dm=deepcopy(diz_patch_Bdm)
                            diz_lavoro_dm[ch][0]=defaultdm0                                
                            Mas0,diz_udpMfinal0,diz_udpCfinal0,diz_patch_B0=self.pandora_mod.cicloModello(self.diz_kappaj,self.diz_patch_udp,self.diz_patch_area,diz_lavoro_dm,self.diz_patch_K,self.diz_patch_M,self.diz_patch_Mmax,self.diz_udp_M2,self.diz_udp_M2max,self.diz_udp_C,self.diz_U,self.diz_h,self.stpedm,self.dlg,"dm",self.costanteA,self.costanteB)
                            listadm.append(Mas0)
                            dm=((self.Mas-Mas0)/self.Mas)*100                                
                            listadm.append(dm)           
                            self.diz_patch_dm[ch]=listadm     
                            
                            
                        if contemp==1:
                            tempodm=time.time() - start_time
                            tempostimato=int((tempodm*(stop_step+1))/60)
                            tempostimato=str(tempostimato)+" minutes"
                            self.aggiorna_output_dm("estimated time\n")
                            self.aggiorna_output_dm(tempostimato)
                            Mas0pr="\npatch,Mas,dm"
                            self.aggiorna_output_dm(Mas0pr)
            
                        try:
                            Mas0
                        except: 
                            Mas0=0
                        try:
                            dm
                        except: 
                            dm=0
                        Mas0pr=str(ch)+","+str(Mas0)+","+str(dm)
                        self.aggiorna_output_dm(Mas0pr)
                    except:
                        pass
                    
                else:
                    listadm=self.diz_patch_dm[ch]
                    contemp+=1
                    self.dlg.progressBar_dm2.setValue(contemp)
                    
                    #per lo stop del dm
                    
                    if self.check_stop_dm==True:
                        return
                    #fine per lo stop del dm
                    
                    if contemp==1:
                        start_time = time.time()                
                    #listadm.append(self.Mas)    
                    if val[0]==0.0:
                        listadm.append(self.Mas)
                        listadm.append(0)
                        self.diz_patch_dm[ch]=listadm
                    else:
        
                        #raw_input("attesa")
                        #import pdb; pdb.set_trace()
                        self.diz_patch_udp,self.diz_patch_area,diz_patch_Bdm,self.diz_patch_K,self.diz_patch_M,self.diz_patch_Mmax,self.diz_udp_M2,self.diz_udp_M2max,self.diz_udp_C,self.diz_patch_clc=self.pandora_mod.inizioModello(self.diz_h,self.diz_U,self.diz_C,self.diz_kappaj,self.diz_confronto,self.diz_permeability,self.diz_perimetri,self.costanteA,self.costanteB)
                        diz_lavoro_dm=deepcopy(diz_patch_Bdm)
                        diz_lavoro_dm[ch][0]=defaultdm0                                
                        Mas0,diz_udpMfinal0,diz_udpCfinal0,diz_patch_B0=self.pandora_mod.cicloModello(self.diz_kappaj,self.diz_patch_udp,self.diz_patch_area,diz_lavoro_dm,self.diz_patch_K,self.diz_patch_M,self.diz_patch_Mmax,self.diz_udp_M2,self.diz_udp_M2max,self.diz_udp_C,self.diz_U,self.diz_h,self.stpedm,self.dlg,"dm",self.costanteA,self.costanteB)
                        listadm.append(Mas0)
                        dm=((self.Mas-Mas0)/self.Mas)*100                                
                        listadm.append(dm)           
                        self.diz_patch_dm[ch]=listadm     
                        
                        
                    if contemp==1:
                        tempodm=time.time() - start_time
                        tempostimato=int((tempodm*(stop_step+1))/60)
                        tempostimato=str(tempostimato)+" minutes"
                        self.aggiorna_output_dm("estimated time\n")
                        self.aggiorna_output_dm(tempostimato)
                        Mas0pr="\npatch,Mas,dm"
                        self.aggiorna_output_dm(Mas0pr)
        
                    try:
                        Mas0
                    except: 
                        Mas0=0
                    try:
                        dm
                    except: 
                        dm=0
                    Mas0pr=str(ch)+","+str(Mas0)+","+str(dm)
                    self.aggiorna_output_dm(Mas0pr)
                        

        
        self.aggiorna_output_dm("\nAnalysis performed\n")
        self.aggiorna_output_dm("----------------\n\n")
        lista=[]
        listapatch=[]
        
        lista.append("cod_patch")
        lista.append(",")
        lista.append("Mtot_as")
        lista.append(",")
        lista.append("dMtot")  
        
        stringlist=''.join(lista)
        listapatch.append(stringlist)         
        
        x=valore=stringlist=None
        lista=[]
        
        diz_x_es=dict()

        contemp=0
        maxdmtot=0
        for key,value in self.diz_patch_dm.iteritems():
            
            if key>=start_step and key<=stop_step:
                
                if self.controllopathcsv==True:

                    try:
                        listapatchcsv.index(int(key))
                        
                        contemp+=1
                        lista.append(str(key))
                        lista.append(",")
                        
                        lista.append(str(value[1]))
                        lista.append(",")
            
                        lista.append(str(value[2]))

            
                        stringlist=''.join(lista)
                        listapatch.append(stringlist)
                        lista=[]
                        diz_x_es[key]=self.diz_patch_clc[key]
                        
                    except:
                        pass

                else:
                    contemp+=1
                    lista.append(str(key))
                    lista.append(",")
                    
                    lista.append(str(value[1]))
                    lista.append(",")
        
                    lista.append(str(value[2]))

                    if value[2]>maxdmtot:
                        maxdmtot=value[2]
        
                    stringlist=''.join(lista)
                    listapatch.append(stringlist)
                    lista=[]
                    diz_x_es[key]=self.diz_patch_clc[key]
                    
                    """
                    ##############
                    #da eliminare
                    if contemp==10:
                        break
                    ##############
                    """


        filef=open(self.path_to_dm, 'wb')

        writer=csv.writer(filef, delimiter='|')
        for item in listapatch:  
            writer.writerow([item]) 
        filef.close()        


        self.controlloES=0
        self.fileescsv=self.dlg.lineEdit_es.text()
        if self.fileescsv!='':
            self.controlloES=1

        ##############da eliminare#############
        self.controlloES=1
        ##############da eliminare#############
        
        #ipdb.set_trace()
        # maxdmtot valore massimo dei dmTOT selezionati
        stringlist=None
        if self.controlloES==1:
            self.diz_patch_es=dict()
            listaes=[]
            listacsves=[]
            listaes.append("cod_patch")
            listaes.append(",")
            listaes.append("belu")
            listaes.append(",")
            listaes.append("area")
            listaes.append(",")
            listaes.append("dMtot")  
            listaes.append(",")
            listaes.append("dMtot_max")
            listaes.append(",")
            listaes.append("ESV_B")  
            stringlist=''.join(listaes)
            listacsves.append(stringlist)  

            listaes=[]



            diz_vc=self.pandora_mod.estraivc(diz_x_es)
            #self.diz_patch_udp
            # print diz_x_es
            # print diz_vc
            # print self.diz_patch_dm(key)
            for key,value in diz_vc.iteritems():
                # print value
                # print self.diz_patch_dm[key][2]
                # print maxdmtot
                # print self.diz_patch_area[key]
                risultatoes=value*(1+(self.diz_patch_dm[key][2]/maxdmtot))*self.diz_patch_area[key]
                #print risultatoes
                self.diz_patch_es[key]=risultatoes
                listaes.append(str(key))
                listaes.append(",")
                listaes.append(str(self.diz_patch_udp[key]))
                listaes.append(",")
                listaes.append(str(self.diz_patch_area[key]))
                listaes.append(",")
                listaes.append(str(self.diz_patch_dm[key][2]))  
                listaes.append(",")
                listaes.append(str(maxdmtot))
                listaes.append(",")
                listaes.append(str(risultatoes))  
                stringlist=''.join(listaes)
                listacsves.append(stringlist) 
                listaes=[]

            filees=open(self.fileescsv, 'wb')
            writeres=csv.writer(filees, delimiter='|')
            for item in listacsves:  
                writeres.writerow([item]) 
            filef.close()  

        else:
            pass

 



                  


    def run(self):
        # show the dialog
        self.dlg.show()
        #self.actionPreferences.triggered.connect(self.configuration_panel())
        self.popolacombo() 
        
        # Run the dialog event loop
        self.result = self.dlg.exec_()
        #print self.result

    
    
