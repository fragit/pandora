PANDORA model assesses the Bio-Energy Landscape Connectivity (BELC) and related Ecosystem
 Services (ESs) for biodiversity conservation. The PANDORA philosophy considers the landscape as
 a unique system, responding to thermodynamic laws, in a state of continuous search for a
 metastable energy equilibrium as a consequence of changes in land use patterns and barrier to
 energy fluxes.

The model was developed to meet the needs of planners and practitioners involved in the 
environmental assessment procedure and it was proposed as an operative decision support system
 to assess the impact of different scenarios of land use change with particular attention to
 urbanization processes. This version of the model, PANDORA 3.0, analyses the contribution of 
each patch of land mosaic to global BELC and, consequently to the functionality and resilience 
of the entire system to which it belongs. Moreover, PANDORA 3.0 allows the selected patches to 
be evaluated in terms of ESs for biodiversity conservation considering both habitat type (as 
defined by information derived from Land use/land cover maps) and BELC.


Details:
Raffaele Pelorosso and Federica Gobattoni. Department of Agricultural and Forestry Sciences
 (DAFNE). Tuscia University - Italy. Developer: Francesco Geri – Sistema Lab snc.