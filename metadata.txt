# This file contains metadata for your plugin. Beginning
# with version 1.8 this is the preferred way to supply information about a
# plugin. The current method of embedding metadata in __init__.py will
# be supported until version 2.0

# This file should be included when you package your plugin.

# Mandatory items:


[general]
name=Pandora
qgisMinimumVersion=2.0
description=Integrated model for the for mathematical analysis of landscape evolution and equilibrium scenarios assessment.
version=2.0.1
author=Università della Tuscia
email=pelorosso@unitus.it

# end of mandatory metadata

# Optional items:

# Uncomment the following line and add your changelog entries:
# changelog=

# tags are comma separated with spaces allowed
tags=

homepage=http://www.dafne.unitus.it/
tracker=https://bitbucket.org/fragit/pandora/issues
repository=https://bitbucket.org/fragit/pandora/src
icon=icon.png
# experimental flag
experimental=Trus

# deprecated flag (applies to the whole plugin, not just a single version
deprecated=False

about=PANDORA was proposed to assess the effects of different planning strategies on final possible equilibrium states that are energetically stable. It provides a tool for the evaluation of landscape functionality and its resilience. 


# Author contact information
author=Raffaele Pelorosso and Federica Gobattoni, Universita' della Tuscia, Viterbo - Dipartimento di scienze e tecnologie per l'Agricoltura, le Foreste, la Natura e l'Energia (DAFNE). Developer: Francesco Geri - Sistema Lab snc.
email=pelorosso@unitus.it

