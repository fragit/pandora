# -*- coding: utf-8 -*-


from PyQt4 import QtCore, QtGui
from ui_tview import Ui_table_border
# create the dialog for zoom to point


class tableDialog(QtGui.QDialog, Ui_table_border):
    def __init__(self,diz_confronto):
        self.diz_confronto=diz_confronto
        QtGui.QDialog.__init__(self)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self,self.diz_confronto)
    
    def richiesta_valori(self):
        self.diz_permeability=self.richiestavalori()
        return self.diz_permeability