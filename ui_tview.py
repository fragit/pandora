# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_tview.ui'
#
# Created: Wed Jan 29 10:28:09 2014
#      by: PyQt4 UI code generator 4.10.3
#
# WARNING! All changes made in this file will be lost!

import sys 
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import * 
from PyQt4.QtGui import *  
import string
import csv
import os, sys

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_table_border(object):
    def setupUi(self, table_border, dati):
        self.dati=dati
        table_border.setObjectName(_fromUtf8("table_border"))
        table_border.resize(450, 278)
        self.table_button = QtGui.QDialogButtonBox(table_border)
        self.table_button.setGeometry(QtCore.QRect(30, 240, 341, 32))
        self.table_button.setOrientation(QtCore.Qt.Horizontal)
        self.table_button.setStandardButtons(QtGui.QDialogButtonBox.Apply|QtGui.QDialogButtonBox.Close|QtGui.QDialogButtonBox.Open|QtGui.QDialogButtonBox.Save)
        self.table_button.setObjectName(_fromUtf8("table_button"))
        self.tableWidget = QtGui.QTableWidget(table_border)
        self.tableWidget.setGeometry(QtCore.QRect(0, 0, 445, 241))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)

        self.retranslateUi(table_border)
        #QtCore.QObject.connect(self.table_button, QtCore.SIGNAL(_fromUtf8("applied()")), self.catturavalori)
        self.table_button.button(QtGui.QDialogButtonBox.Apply).clicked.connect(self.catturavalori)
        self.table_button.button(QtGui.QDialogButtonBox.Open).clicked.connect(self.caricavalori)
        self.table_button.button(QtGui.QDialogButtonBox.Save).clicked.connect(self.salvavalori)
        QtCore.QObject.connect(self.table_button, QtCore.SIGNAL(_fromUtf8("rejected()")), table_border.reject)        
        QtCore.QMetaObject.connectSlotsByName(table_border)
        
        self.popolatable()
        

    def retranslateUi(self, table_border):
        table_border.setWindowTitle(_translate("table_border", "Border Permeability Settings", None))
        
    def popolatable(self):
        
        self.col=len(self.dati)
                
        self.valorezero="0.000"
    
        self.tableWidget.setRowCount(self.col)
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setHorizontalHeaderLabels(['First BELU_i', 'First BELU_k','Length','Permeability'])
        self.c=0
        for self.udp_key,self.udp_value in sorted(self.dati.iteritems()):
            self.lista_coppia=string.split(self.udp_key, "-")
            
            item1 = QTableWidgetItem(str(self.lista_coppia[0]))   
            item2 = QTableWidgetItem(str(self.lista_coppia[1]))   
            item_l = QTableWidgetItem(str(self.udp_value))
            item_0 = QTableWidgetItem(self.valorezero)   
            item1.setFlags(QtCore.Qt.ItemIsEnabled)
            item2.setFlags(QtCore.Qt.ItemIsEnabled)
            item_l.setFlags(QtCore.Qt.ItemIsEnabled) #disabilitare editing
            self.tableWidget.setItem(self.c, 0, item1) 
            self.tableWidget.setItem(self.c, 1, item2)
            self.tableWidget.setItem(self.c, 2, item_l)
            self.tableWidget.setItem(self.c, 3, item_0)     
            self.c+=1       
            
            
    def catturavalori(self):
        self.diz_perm=dict()
        for self.c in range(0,self.tableWidget.rowCount()):
            
            #self.diz_perm[self.table.item(self.c,0).text()]=self.table.item(self.c,1).text()
            self.udp_primo1= str(self.tableWidget.item(self.c,0).text())
            self.udp_secondo1= str(self.tableWidget.item(self.c,1).text())
            self.permeability1= str(self.tableWidget.item(self.c,3).text())
            self.coppia_udp=str(self.udp_primo1)+"-"+str(self.udp_secondo1)
            self.diz_perm[self.coppia_udp]=self.permeability1  
        QtGui.QMessageBox.information(self,"Info", "Permeability values were saved") 
                    
        #print self.diz_perm
    
    def richiestavalori(self):
        return self.diz_perm
    
    def salvavalori(self):
        self.sname = QFileDialog.getSaveFileName(None, 'Save file', '/home','csv files (*.csv);;all files (*.*)')
        #print self.sname
        if os.path.exists(self.sname):
            os.remove(self.sname) 
        self.permeability_s=[]
        stringlist=[]
        listasalvaperm=[]


        for self.sc in range(0,self.tableWidget.rowCount()):
            udp_primo1= str(self.tableWidget.item(self.sc,0).text())
            udp_secondo1= str(self.tableWidget.item(self.sc,1).text())
            coppia_udp=str(udp_primo1)+"-"+str(udp_secondo1)
            self.permeability_s.append(coppia_udp)
            self.permeability_s.append(",")
            self.permeability_s.append(self.tableWidget.item(self.sc,2).text())
            self.permeability_s.append(",")
            self.permeability_s.append(self.tableWidget.item(self.sc,3).text())
            stringlist=''.join(self.permeability_s)
            listasalvaperm.append(stringlist)
            self.permeability_s=[]
        #print self.permeability_s           
        try:
            self.sfilecsv=open(self.sname, 'wb')
        except:
            return
        #self.writer=csv.writer(self.sfilecsv, delimiter='|')
        self.writer=csv.writer(self.sfilecsv, delimiter='|',quoting=csv.QUOTE_MINIMAL, dialect='excel')
        #csv.writer(open("myfile.csv","w"), delimiter=',',quoting=csv.QUOTE_ALL)
        for self.item in listasalvaperm:  
            self.writer.writerow([self.item])

        self.sfilecsv.close()
       
        
    
    def caricavalori(self):
        self.fname = QFileDialog.getOpenFileName(None, 'Open file', '/home','csv files (*.csv);;all files (*.*)')
        try:
            self.filecsv=open(self.fname,"r")
        except:
            return        
            
        self.lettore=csv.reader(self.filecsv, delimiter=',')
        self.contatorefile=0
        
        for self.riga in self.lettore:
            self.contatorefile+=1
        if self.contatorefile!=self.tableWidget.rowCount():
            QtGui.QMessageBox.critical(self,"Error", "Error number of rows")
            return
        self.filecsv.close()
        self.lettore=self.riga=self.contatorefile=None
        self.contatorefile=0
        self.filecsv=open(self.fname,"r")        
        self.lettore=csv.reader(self.filecsv,delimiter=',')
        for self.riga in self.lettore:                
            item_p = QTableWidgetItem(self.riga[1])
            self.tableWidget.setItem(self.contatorefile, 3, item_p)   
            self.contatorefile+=1    
               

        

