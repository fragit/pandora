import sys 
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import * 
from PyQt4.QtGui import *  

#global window_border

class MyTable(QTableWidget): 
  def __init__(self, data, *args): 
    QTableWidget.__init__(self, *args) 
    self.setMinimumSize(200, 185)
    self.data = data 
    self.setmydata() 
    self.resizeColumnsToContents() 
    self.resizeRowsToContents()
     

    
  def setmydata(self):       
    
    for i in range(0,len(self.data)):
      newitem = QTableWidgetItem(self.data[i])
      newitem.setFlags(QtCore.Qt.ItemIsEnabled)
      item0=QTableWidgetItem('0.000')
      self.setItem(i, 0, newitem)
      self.setItem(i,1,item0)
      
    self.instestazioni=['confine','permeability']
    self.setHorizontalHeaderLabels(self.instestazioni)   

class border_window():
  def __init__(self,data,nrighe):
      self.data=data
      
      self.valorezero=['0.0000']
      
      self.y=nrighe    #numero confini trovati
      
      self.app= QtGui.QApplication([])
      window_border= QtGui.QMainWindow()
      self.palette= QtGui.QPalette()
      self.VLayout1= QtGui.QVBoxLayout()
      self.HLayout1=QtGui.QHBoxLayout()
      self.centralWidget = QtGui.QWidget()
      self.table = MyTable(self.data, self.y, 2) 
      

      
      self.VLayout1.addWidget(self.table)
      self.VLayout1.addLayout(self.HLayout1)
      self.button_apply=QtGui.QPushButton("apply")
      self.button_apply.setMaximumWidth(50)
      self.button_apply.clicked.connect(self.cattura_valori)
      self.button_close=QtGui.QPushButton("close")
      self.button_close.setMaximumWidth(50)
      self.button_close.clicked.connect(self.chiudi)
      self.HLayout1.addWidget(self.button_apply)
      self.HLayout1.addWidget(self.button_close)
      
      window_border.setCentralWidget(self.centralWidget)
      self.centralWidget.setLayout(self.VLayout1)

      window_border.resize(200,200)
      window_border.setWindowTitle('Border analysis process')
      window_border.show()
      
      self.app.exec_()
      
  def cattura_valori(self):

    self.diz_perm=dict()
    for self.c in range(0,self.table.rowCount()):
      #self.diz_perm[self.table.item(self.c,0).text()]=self.table.item(self.c,1).text()
      self.coppia= str(self.table.item(self.c,0).text())
      self.valore= str(self.table.item(self.c,1).text())
      self.diz_perm[self.coppia]=self.valore
    print self.diz_perm
      
 
    #QtCore.SLOT('close()')
    
  def chiudi(self):
    self.app.quit()