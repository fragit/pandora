# -*- coding: utf-8 -*-
"""
/***************************************************************************
 pandora
                                 A QGIS plugin
 Pandora
                             -------------------
        begin                : 2014-01-07
        copyright            : (C) 2014 by Università della Tusca
        email                : geri.francesco@zoho.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

def classFactory(iface):
    # load pandora class from file pandora
    from pandora import pandora
    return pandora(iface)
