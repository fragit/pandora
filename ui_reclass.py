# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_reclass.ui'
#
# Created: Mon Jan 20 11:32:19 2014
#      by: PyQt4 UI code generator 4.10.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    from osgeo import gdal,ogr
except:
    print "librerie gdal/ogr non trovate"
    
try:
    import numpy
    from numpy import zeros
    from numpy import logical_and    
except:
    print "librerie numpy non trovate"      

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_reclass(object):
    def setupUi(self, reclass):
        reclass.setObjectName(_fromUtf8("reclass"))
        reclass.resize(400, 300)
        self.buttonBox = QtGui.QDialogButtonBox(reclass)
        self.buttonBox.setGeometry(QtCore.QRect(40, 260, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayoutWidget = QtGui.QWidget(reclass)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 381, 251))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_2 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_3.addWidget(self.label_2)
        self.input_reclass = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.input_reclass.setObjectName(_fromUtf8("input_reclass"))
        self.horizontalLayout_3.addWidget(self.input_reclass)
        self.button_in_reclass = QtGui.QPushButton(self.verticalLayoutWidget)
        self.button_in_reclass.setObjectName(_fromUtf8("button_in_reclass"))
        self.horizontalLayout_3.addWidget(self.button_in_reclass)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.label_3 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.verticalLayout.addWidget(self.label_3)
        
        self.tablet_raster_class = QtGui.QTableWidget(self.verticalLayoutWidget)
        self.tablet_raster_class.setObjectName(_fromUtf8("tablet_raster_class"))
        self.tablet_raster_class.setColumnCount(2)
        self.tablet_raster_class.setRowCount(1)
        self.tablet_raster_class.setHorizontalHeaderLabels(['Value', 'Reclass value'])
        
        self.verticalLayout.addWidget(self.tablet_raster_class)
        self.horizontalLayout_13 = QtGui.QHBoxLayout()
        self.horizontalLayout_13.setSpacing(0)
        self.horizontalLayout_13.setContentsMargins(-1, 0, -1, -1)
        self.horizontalLayout_13.setObjectName(_fromUtf8("horizontalLayout_13"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_13.addItem(spacerItem)
        
        self.button_add_row = QtGui.QPushButton(self.verticalLayoutWidget)
        self.button_add_row.setMaximumSize(QtCore.QSize(70, 16777215))
        self.button_add_row.setObjectName(_fromUtf8("button_add_row"))
        self.button_add_row.clicked.connect(self.aggiungi)
        
        self.horizontalLayout_13.addWidget(self.button_add_row)
        
        self.button_delete_row = QtGui.QPushButton(self.verticalLayoutWidget)
        self.button_delete_row.setMaximumSize(QtCore.QSize(70, 16777215))
        self.button_delete_row.setObjectName(_fromUtf8("button_delete_row"))
        self.button_delete_row.clicked.connect(self.togli)
        
        self.horizontalLayout_13.addWidget(self.button_delete_row)
        self.verticalLayout.addLayout(self.horizontalLayout_13)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label = QtGui.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_4.addWidget(self.label)
        self.output_raster_class = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.output_raster_class.setObjectName(_fromUtf8("output_raster_class"))
        self.horizontalLayout_4.addWidget(self.output_raster_class)
        self.button_out_reclass = QtGui.QPushButton(self.verticalLayoutWidget)
        self.button_out_reclass.setObjectName(_fromUtf8("button_out_reclass"))
        self.horizontalLayout_4.addWidget(self.button_out_reclass)
        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.retranslateUi(reclass)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), self.cattura_valori)
        #self.buttonBox.button(QtGui.QDialogButtonBox.Apply).clicked.connect(self.catturavalori)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), reclass.reject)
        QtCore.QMetaObject.connectSlotsByName(reclass)
        
    def aggiungi(self):
        self.tablet_raster_class.insertRow(self.tablet_raster_class.rowCount())
        
    def togli(self):
        self.tablet_raster_class.removeRow(1)   
        
    def cattura_valori(self):
        self.listavalues=[]
        self.listaclass=[]
        for self.c in range(0,self.tablet_raster_class.rowCount()):
            #self.diz_perm[self.table.item(self.c,0).text()]=self.table.item(self.c,1).text()
            try:
                self.listavalues.append(str(self.tablet_raster_class.item(self.c,0).text()))
                self.listaclass.append(str(self.tablet_raster_class.item(self.c,1).text()))   
            except:
                pass   
        #print self.listavalues,self.listaclass 
        self.in_file=self.input_reclass.text()
        self.out_file=self.output_raster_class.text()
        self.k1=''
        self.k2=''
        self.classification_values=map(float, self.listavalues)
        self.classification_output_values=map(float, self.listaclass)
              
        self.crea_raster()
        
        
    def crea_raster(self):
       
        self.controllo_creaesp=False
        #self.classification_values = [0,45,225,315,360] #i valori da classificare 
        #self.classification_output_values = [0,1,0.5,0,0] #I valori di classificazione

  
        #self.in_file = "/home/francesco/Lavori/tuscia/raster/esp.tif" 
        #input file
        
        self.result_creaesp="Reclassification aspect map\n\n"
        
        self.ds = gdal.Open(self.in_file)
        try:
            self.band = self.ds.GetRasterBand(1)
        except:
            self.controllo=True
            self.result_creaesp+="Aspect map not valid\n"
            self.result_creaesp+="Errors in reclassification\n"
            self.result_creaesp+="---------------------------\n"  
            return self.result_creaesp,self.controllo
        
        self.block_sizes = self.band.GetBlockSize()
        self.x_block_size = self.block_sizes[0]
        self.y_block_size = self.block_sizes[1]
        
        self.xsize = self.band.XSize
        self.ysize = self.band.YSize
        
        self.max_value = self.band.GetMaximum()
        self.min_value = self.band.GetMinimum()
        
        if self.max_value == None or self.min_value == None:
            self.stats = self.band.GetStatistics(0, 1)
            self.max_value = self.stats[1]
            self.min_value = self.stats[0]
        
        self.format = "GTiff" #formato di uscita Gtiff per forza perchè gdal non scrive altro
        self.driver = gdal.GetDriverByName( self.format )
        self.dst_ds = self.driver.Create(self.out_file, self.xsize, self.ysize, 1, gdal.GDT_Float32  ) 
        #file di uscita attenzione all'ultimo parametro Gdaltypefile
        #crea un immagine che verrà riutilizzata nelle analisi successive
        #e che verrà salvata nella directory di lavoro
        
        self.dst_ds.SetGeoTransform(self.ds.GetGeoTransform())
        self.dst_ds.SetProjection(self.ds.GetProjection())
        
        for self.i in range(0, self.ysize, self.y_block_size):
            if self.i + self.y_block_size < self.ysize:
                self.rows = self.y_block_size
            else:
                self.rows = self.ysize - self.i
            for self.j in range(0, self.xsize, self.x_block_size):
                if self.j + self.x_block_size < self.xsize:
                    self.cols = self.x_block_size
                else:
                    self.cols = self.xsize
        
                self.data = self.band.ReadAsArray(self.j, self.i, self.cols, self.rows)
                self.r = zeros((self.rows, self.cols), numpy.uint8)
        
                for self.k in range(len(self.classification_values) - 1):
                    if self.classification_values[self.k] < self.max_value and (self.classification_values[self.k + 1] > self.min_value ):
                        self.r = self.r + self.classification_output_values[self.k] * logical_and(self.data >= self.classification_values[self.k], self.data < self.classification_values[self.k + 1])
                if self.classification_values[self.k + 1] < self.max_value:
                    self.r = self.r + self.classification_output_values[self.k] * (self.data >= self.classification_values[self.k + 1])
        
                self.dst_ds.GetRasterBand(1).WriteArray(self.r,self.j,self.i)
        
        self.dst_ds = None
        
        self.result_creaesp+="Reclassification aspect map performed\n"
        self.result_creaesp+="\n---------------------------\n"
        return self.result_creaesp,self.controllo_creaesp           
        
    
        

    def retranslateUi(self, reclass):
        reclass.setWindowTitle(_translate("reclass", "Raster reclassification", None))
        self.label_2.setText(_translate("reclass", "Input raster layer", None))
        self.button_in_reclass.setText(_translate("reclass", "Open", None))
        self.label_3.setText(_translate("reclass", "Classification values", None))
        self.button_add_row.setText(_translate("reclass", "Add", None))
        self.button_delete_row.setText(_translate("reclass", "Delete", None))
        self.label.setText(_translate("reclass", "Output raster layer", None))
        self.button_out_reclass.setText(_translate("reclass", "Save", None))

